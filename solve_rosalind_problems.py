import helper_scripts.classloader


class SolveRoselindProblems:
    def __init__(self):
        self.filefoldermanager = filefoldermanagement.FileFolderManagement()
        self.bio_class = bioinformaticsfunctions.BioinformaticsFunctions()
        self.format_txt = formatvalue.FormatValue()

    def load_and_return_content(self, file_name):
        input_file_address = script_properties["resources"]["file_folder_address"]["roselind_problems"]
        input_file_address += file_name
        content = self.filefoldermanager.read_file_into_array_by_line(input_file_address)
        return content

    def load_and_return_raw_content(self, file_name):
        input_file_address = script_properties["resources"]["file_folder_address"]["roselind_problems"]
        input_file_address += file_name
        content = self.filefoldermanager.read_file_to_memory(input_file_address)

        return content

    def load_and_return_content_delimted(self, file_name, delim):
        input_file_address = script_properties["resources"]["file_folder_address"]["roselind_problems"]
        input_file_address += file_name
        content = self.filefoldermanager.read_file_into_array_by_delim(input_file_address, delim)
        return content

    def write_content_to_file(self, file_name, content):
        file_name = file_name.replace(".txt", "_result.txt")
        output_file_address = script_properties["resources"]["file_folder_address"]["roselind_results"]
        output_file_address += file_name
        self.filefoldermanager.write_content_to_file(content, output_file_address, True)

    def count_nucleotide_from_file(self, file_name):
        content = self.load_and_return_content(file_name)
        result_arr = []
        for seq in content:
            temp_result_str = ""
            temp_result_arr = self.bio_class.count_nucleotide(seq)
            itr_arr = ["A", "C", "G", "T"]
            for nuc in itr_arr:
                temp_result_str += str(temp_result_arr[nuc]) + " "
            result_arr.append(temp_result_str[:temp_result_str.rindex(" ")])
        result_str = self.format_txt.list_to_string_delim_linebreak(result_arr)
        self.write_content_to_file(file_name, result_str)

    def convert_dna_to_rna(self, file_name):
        content = self.load_and_return_content(file_name)
        result_arr = []
        for seq in content:
            temp_result_str = self.bio_class.convert_to_rna(seq)
            result_arr.append(temp_result_str)
        result_str = self.format_txt.list_to_string_delim_linebreak(result_arr)
        self.write_content_to_file(file_name, result_str)

    def generate_complementing_strand_of_dna(self, file_name):
        content = self.load_and_return_content(file_name)
        result_arr = []
        for seq in content:
            temp_result_str = self.bio_class.create_complementing_strand_of_dna(seq)
            result_arr.append(temp_result_str)
        result_str = self.format_txt.list_to_string_delim_linebreak(result_arr)
        self.write_content_to_file(file_name, result_str)

    def generate_rabbits_and_recurrence_relations(self, file_name):
        content = self.load_and_return_content(file_name)
        result_arr = []
        for seq in content:
            temp_result_str = "\nTable for pair -> %s \n" % seq
            current_pair = seq.split(" ")
            temp_result_str += self.bio_class.return_rabbits_and_recurrence_relations(int(current_pair[0]),
                                                                                      int(current_pair[1]))
            result_arr.append(temp_result_str)
            print(temp_result_str)
        result_str = self.format_txt.list_to_string_delim_linebreak(result_arr)
        self.write_content_to_file(file_name, result_str)

    def generate_mortal_fibonacci_rabbits(self, file_name):
        content = self.load_and_return_content(file_name)
        result_arr = []
        for seq in content:
            temp_result_str = "\nTable for pair -> %s \n" % seq
            current_pair = seq.split(" ")
            temp_result_str += self.bio_class.return_mortal_fibonacci_rabbits(int(current_pair[0]),
                                                                              int(current_pair[1]))
            result_arr.append(temp_result_str)
        result_str = self.format_txt.list_to_string_delim_linebreak(result_arr)
        self.write_content_to_file(file_name, result_str)

    def count_g_c_content(self, file_name):
        content = self.load_and_return_content_delimted(file_name, ">")
        result_dict = {}
        input_dict = {}
        for v_val in content:
            value_arr = v_val.split("\n")
            input_dict[value_arr[0]] = "".join([itm for itm in value_arr if len(itm) > 0 and itm != value_arr[0]])
        for k_key in input_dict.keys():
            temp_dict = self.bio_class.count_nucleotide(input_dict[k_key])
            all_nuc = 0
            gc_nuc = 0
            for kk_key in temp_dict:
                all_nuc += temp_dict[kk_key]
                if kk_key == "G" or kk_key == "C":
                    gc_nuc += temp_dict[kk_key]
                gc_percent = gc_nuc / all_nuc
                gc_percent *= 100
            result_dict[k_key] = gc_percent
        high_gc = ["", 0]
        for k_key in result_dict:
            if result_dict[k_key] >= high_gc[1]:
                high_gc[0] = k_key
                high_gc[1] = result_dict[k_key]
        result_str = high_gc[0] + " " + str(high_gc[1]) + "%"
        print(result_dict)
        print(result_str)
        self.write_content_to_file(file_name, result_str)

    def find_counting_point_mutations(self, file_name):
        content = self.load_and_return_content(file_name)
        str_one = list(content[0])
        str_two = list(content[1])
        str_len = len(str_one)
        compare_hash = []
        diff_counter = 0
        for idx_x in range(str_len):
            concordence = "Y" if str_one[idx_x] == str_two[idx_x] else "N"
            if concordence == "N":
                diff_counter += 1
            compare_hash.append(" ".join([str_one[idx_x], str_two[idx_x], concordence, str(idx_x), str(diff_counter)]))
        result_str = self.format_txt.list_to_string_delim_linebreak(compare_hash)
        print(result_str)
        self.write_content_to_file(file_name, result_str)

    def find_mandels_law(self, file_name):
        file_content = self.load_and_return_content(file_name)
        for content in file_content:
            total_obj = content.split(" ")
            itm_rng = len(total_obj)
            itms_dict = {}
            result_dict = {}
            total_obj_count = 0
            for itm in range(itm_rng):
                itms_dict[chr(65 + itm)] = int(total_obj[itm])
                total_obj_count += itms_dict[chr(65 + itm)]
            itm_type = itms_dict.keys()
            unq_pair = [(x, y) for x in itm_type for y in itm_type]
            unq_pair_dup = list(unq_pair)
            # for pair in unq_pair_dup:
            #    if pair[0] != pair[1]:
            #        if (pair[1], pair[0]) in unq_pair:
            #            unq_pair.remove(pair)
            total_percent = 0
            for pair in unq_pair:
                temp_arr = []
                temp_itm_dict = dict(itms_dict)
                temp_itm_dict_keys = temp_itm_dict.keys()
                # while temp_itm_dict[pair[0]] > 0 and temp_itm_dict[pair[1]]:
                count_total = 0
                for k_key in temp_itm_dict_keys:
                    count_total += temp_itm_dict[k_key]
                comb_value = temp_itm_dict[pair[0]] / count_total
                temp_itm_dict[pair[0]] -= 1
                count_total = 0
                for k_key in temp_itm_dict_keys:
                    count_total += temp_itm_dict[k_key]
                comb_value *= temp_itm_dict[pair[1]] / count_total
                temp_itm_dict[pair[1]] -= 1
                temp_arr.append(comb_value)
                pair_percent = 0
                for idx in temp_arr:
                    pair_percent += idx
                total_percent += pair_percent
                result_dict["".join(pair)] = pair_percent
            result_keys = result_dict.keys()
            dom_percent = 0
            total_percent = 0
            score = {"AA": 1,
                     "AB": 1,
                     "AC": 1,
                     "BB": 0.75,
                     "BA": 1,
                     "BC": 0.5,
                     "CC": 0,
                     "CA": 1,
                     "CB": 0.5}
            for k_key in result_keys:
                pair_score = score[k_key]
                dom_percent += pair_score * result_dict[k_key]
                total_percent += result_dict[k_key]
            print(dom_percent)
            print(total_percent)
            print(dom_percent / total_percent)

    def generate_rna_to_protein(self, file_name):
        file_content = self.load_and_return_content(file_name)
        protein_str = ""
        result_arr = []
        for content in file_content:
            result_arr.append(self.bio_class.rna_to_protein(content))
        result_str = self.format_txt.list_to_string_delim_linebreak(result_arr)
        self.write_content_to_file(file_name, result_str)

    def find_dna_motif(self, file_name):
        file_content = self.load_and_return_content(file_name)
        str_one = list(file_content[0])
        str_two = file_content[1]
        str_one_len = len(str_one)
        str_two_len = len(str_two)
        cur_loci = 0
        result_arr = []
        while cur_loci < str_one_len:
            cur_str = "".join(str_one[cur_loci:cur_loci + str_two_len])
            if str_two == cur_str:
                result_arr.append(str(cur_loci + 1))
            cur_loci += 1
        result_str = " ".join(result_arr)
        self.write_content_to_file(file_name, result_str)
        print(result_str)

    def create_dna_profile(self, file_name):
        content = self.load_and_return_content_delimted(file_name, ">")
        input_dict = {}
        for v_val in content:
            value_arr = v_val.split("\n")
            input_dict[value_arr[0]] = "".join([itm for itm in value_arr if len(itm) > 0 and itm != value_arr[0]])
        matrix_arr = []
        for k_key in input_dict.keys():
            matrix_arr.append(list(input_dict[k_key]))
        seq_len = 0
        for itm in matrix_arr:
            c_item_len = len(itm)
            if seq_len == 0 or seq_len > c_item_len:
                seq_len = c_item_len
        result_mtrx_keys = ["A", "C", "G", "T"]
        result_mtrx_keys_len = len(result_mtrx_keys)
        result_mtrx = {}
        for idx_i in range(result_mtrx_keys_len):
            result_mtrx[result_mtrx_keys[idx_i]] = [0] * seq_len
        for itm in matrix_arr:
            for i_chr in range(seq_len):
                for idx_i in range(result_mtrx_keys_len):
                    if result_mtrx_keys[idx_i] == itm[i_chr]:
                        result_mtrx[result_mtrx_keys[idx_i]][i_chr] += 1
        consensus_str = ""
        for i_chr in range(seq_len):
            temp_count = 0
            temp_chr = ""
            for idx_i in range(result_mtrx_keys_len):
                if result_mtrx[result_mtrx_keys[idx_i]][i_chr] >= temp_count:
                    temp_count = result_mtrx[result_mtrx_keys[idx_i]][i_chr]
                    temp_chr = result_mtrx_keys[idx_i]
            consensus_str += temp_chr
        result_arr = [consensus_str]
        for idx_i in range(result_mtrx_keys_len):
            result_arr.append(
                result_mtrx_keys[idx_i] + ": " + ' '.join(str(x) for x in result_mtrx[result_mtrx_keys[idx_i]]))
        result_str = self.format_txt.list_to_string_delim_linebreak(result_arr)
        self.write_content_to_file(file_name, result_str)
        print(result_str)

    @staticmethod
    def loop_test():

        for x in range(100):
            print(x)
        for x in range(0, 100):
            print("- " + str(x))

    def create_overlap_graphs(self, file_name):
        ts = time.time()
        st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
        result_arr = []
        print("Started Processing -> " + st)
        content = self.load_and_return_content_delimted(file_name, ">")
        input_dict = {}
        for v_val in content:
            value_arr = v_val.split("\n")
            input_dict[value_arr[0]] = "".join([itm for itm in value_arr if len(itm) > 0 and itm != value_arr[0]])
        core_dict = {}
        input_keys = sorted(list(input_dict.keys()))
        for k_key in input_keys:
            core_dict[k_key] = list(input_dict[k_key])
        input_range = len(input_keys)
        loci_arr = [[]] * input_range
        for idx_i in range(0, input_range):
            loci_arr[idx_i] = [[]] * len(core_dict[input_keys[idx_i]])
        for idx_i in range(0, input_range):
            nuc_rng = len(loci_arr[idx_i])
            for nuc in range(nuc_rng):
                loci_arr[idx_i][nuc] = [[]] * input_range
        for idx_i in range(0, input_range):
            seq_one_range = len(core_dict[input_keys[idx_i]])
            for idx_i_i in range(idx_i + 1, input_range):
                if idx_i != idx_i_i:
                    seq_two_range = len(core_dict[input_keys[idx_i_i]])
                    for nuc_x in range(0, seq_one_range):
                        loci_local_arr = list(loci_arr[idx_i][nuc_x][idx_i_i])
                        for nuc_y in range(0, seq_two_range):
                            match_size = 1
                            comp_tup_1 = core_dict[input_keys[idx_i]][nuc_x]
                            comp_tup_2 = core_dict[input_keys[idx_i_i]][nuc_y]
                            while comp_tup_1 == comp_tup_2 \
                                    and nuc_x + match_size <= seq_one_range \
                                    and nuc_y + match_size <= seq_two_range:
                                match_size += 1
                                comp_tup_1 = core_dict[input_keys[idx_i]][nuc_x:nuc_x + match_size]
                                comp_tup_2 = core_dict[input_keys[idx_i_i]][nuc_y:nuc_y + match_size]
                            if match_size >= 3:
                                if len(loci_arr[idx_i][nuc_x][idx_i_i]) < 1 or \
                                                loci_arr[idx_i][nuc_x][idx_i_i][1] < match_size:
                                    loci_local_arr.append([nuc_y, match_size - 1])
                        loci_arr[idx_i][nuc_x][idx_i_i] = loci_local_arr

        ts = time.time()
        st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
        print("Complete Time -> " + st)

        output_file_address = "C:\\Users\\mhaque8672\\Desktop\\temp_out_put\\Loop_2_result.txt"
        for idx_i in range(0, input_range):
            seq_one_range = len(core_dict[input_keys[idx_i]])
            for idx_i_i in range(idx_i + 1, input_range):
                if idx_i != idx_i_i:
                    for nuc_x in range(0, seq_one_range):
                        seq_two_match_rng = len(loci_arr[idx_i][nuc_x][idx_i_i])
                        # if seq_two_match_rng > 0 and loci_arr[idx_i][nuc_x][idx_i_i][1] >= 8:
                        if seq_two_match_rng > 0:
                            for match in range(0, seq_two_match_rng):
                                c_match = loci_arr[idx_i][nuc_x][idx_i_i][match]
                                # if nuc_x == 0 and c_match[0] + c_match[1] == len(core_dict[input_keys[idx_i_i]]):
                                if c_match[1] == 3:
                                    if nuc_x == 0 or c_match[0] == 0:
                                        if nuc_x + c_match[1] == len(core_dict[input_keys[idx_i]]) or \
                                                                c_match[0] + c_match[1] \
                                                        == len(core_dict[input_keys[idx_i_i]]):
                                            result_str_one = ""
                                            result_str_two = ""
                                            if nuc_x < c_match[0]:
                                                result_str_one = " " * (c_match[0] - nuc_x)
                                            else:
                                                result_str_two = " " * (nuc_x - c_match[0])
                                            result_str_one = input_keys[idx_i] + "  " + result_str_one + "".join(
                                                core_dict[input_keys[idx_i]])
                                            result_str_two = input_keys[idx_i_i] + "  " + result_str_two + "".join(
                                                core_dict[input_keys[idx_i_i]][0:c_match[0]]).lower() + "".join(
                                                core_dict[input_keys[idx_i_i]][c_match[0]:c_match[0] + c_match[1]]) + \
                                                             "".join(
                                                                 core_dict[input_keys[idx_i_i]][
                                                                 c_match[0] + c_match[1]:len(
                                                                     core_dict[input_keys[idx_i_i]])]).lower()
                                            content = result_str_one + "\n" + result_str_two + "\n"
                                            # print(content)
                                            # print("###")
                                            if c_match[0] == 0 and nuc_x + c_match[1] == len(
                                                    core_dict[input_keys[idx_i]]):
                                                content = input_keys[idx_i] + " " + input_keys[idx_i_i] + "\n" + content
                                                result_arr.append(input_keys[idx_i] + " " + input_keys[idx_i_i])

                                            if nuc_x == 0 and c_match[0] + c_match[1] == len(
                                                    core_dict[input_keys[idx_i_i]]):
                                                content = input_keys[idx_i_i] + " " + input_keys[idx_i] + "\n" + content
                                                result_arr.append(input_keys[idx_i_i] + " " + input_keys[idx_i])
                                            print("###")
                                            print(content)
                                            print("###")

        result_str = self.format_txt.list_to_string_delim_linebreak(result_arr)
        self.write_content_to_file(file_name, result_str)
        # print(result_str)
        # self.filefoldermanager.write_content_to_file(content, output_file_address, False)

    def calculating_expected_offspring(self, file_name):
        file_content = self.load_and_return_content(file_name)
        pop_list = file_content[0].split(" ")
        geno_type_list = {"0": ["AA", "AA"],
                          "1": ["AA", "Aa"],
                          "2": ["AA", "aa"],
                          "3": ["Aa", "Aa"],
                          "4": ["Aa", "aa"],
                          "5": ["aa", "aa"]}
        geno_key_list = geno_type_list.keys()
        offspring_mapped = {}
        for k_key in geno_key_list:
            offspring_mapped[k_key] = self.bio_class.create_offspring_array(geno_type_list[k_key])
        dom_count = 0
        caculated_score = {}
        for k_key in geno_key_list:
            dom_count = 0
            for offspring in offspring_mapped[k_key]:
                if "A" in offspring:
                    dom_count += 1
            caculated_score[k_key] = dom_count / len(offspring_mapped[k_key])
        offspring_count = 2
        total_offspring = 0
        for idx_i in range(len(pop_list)):
            total_offspring += ((caculated_score[str(idx_i)] * offspring_count) * int(pop_list[idx_i]))
            # print((caculated_score[str(idx_i)] * offspring_count) * int(pop_list[idx_i]))
        print(total_offspring)
        result_str = str(total_offspring)
        self.write_content_to_file(file_name, result_str)

        # print(caculated_score)
        # score = {"0": 1,        # AA-AA
        #          "1": 1,        # AA-Aa
        #          "2": 1,        # AA-aa
        #          "3": 0.75,     # Aa-Aa
        #          "4": 1,        # Aa-aa
        #          "5": 0.5}      # aa-aa
        #
        # #print(pop_list)

    @staticmethod
    def print_current_time(attach_text):
        ts = time.time()
        st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
        print(attach_text + st)

    def finding_a_shared_motif(self, file_name):
        self.print_current_time("Started Processing.")
        result_arr = []
        content = self.load_and_return_content_delimted(file_name, ">")
        input_dict = {}
        for v_val in content:
            value_arr = v_val.split("\n")
            input_dict[value_arr[0]] = "".join([itm for itm in value_arr if len(itm) > 0 and itm != value_arr[0]])
        core_dict = {}
        input_keys = sorted(list(input_dict.keys()))
        for k_key in input_keys:
            core_dict[k_key] = list(input_dict[k_key])
        input_range = len(input_keys)
        loci_arr = [[]] * input_range
        for idx_i in range(0, input_range):
            loci_arr[idx_i] = [[]] * len(core_dict[input_keys[idx_i]])
        for idx_i in range(0, input_range):
            nuc_rng = len(loci_arr[idx_i])
            for nuc in range(nuc_rng):
                loci_arr[idx_i][nuc] = [[]] * input_range
        for idx_i in range(0, 1):
            seq_one_range = len(core_dict[input_keys[idx_i]])
            for idx_i_i in range(idx_i + 1, idx_i + 2):
                if idx_i != idx_i_i:
                    seq_two_range = len(core_dict[input_keys[idx_i_i]])
                    nuc_x = 0
                    while nuc_x < seq_one_range:
                        loci_local_arr = list(loci_arr[idx_i][nuc_x][idx_i_i])
                        nuc_y = 0
                        while nuc_y < seq_two_range:
                            match_size = 0
                            while core_dict[input_keys[idx_i]][nuc_x:nuc_x + match_size] == core_dict[
                                                                                                input_keys[idx_i_i]][
                                                                                            nuc_y:nuc_y + match_size] \
                                    and nuc_x + match_size <= seq_one_range \
                                    and nuc_y + match_size <= seq_two_range:
                                match_size += 1
                            if match_size >= 3:
                                loci_local_arr.append([nuc_y, match_size - 1])
                                nuc_y += match_size
                            else:
                                nuc_y += 1
                        loci_arr[idx_i][nuc_x][idx_i_i] = loci_local_arr
                        nuc_x += 1
        self.print_current_time("Sequence process complete.")

        common_str_arr = []
        largest_str = 0
        self.print_current_time("Started parsing matches ")
        for idx_i in range(0, input_range):
            seq_one_range = len(core_dict[input_keys[idx_i]])
            for idx_i_i in range(idx_i + 1, input_range):
                if idx_i != idx_i_i:
                    for nuc_x in range(0, seq_one_range):
                        seq_two_match_rng = len(loci_arr[idx_i][nuc_x][idx_i_i])
                        if seq_two_match_rng > 0:
                            for match in range(0, seq_two_match_rng):
                                c_match = loci_arr[idx_i][nuc_x][idx_i_i][match]
                                result_str = "".join(core_dict[input_keys[idx_i_i]][c_match[0]:c_match[0] + c_match[1]])
                                try:
                                    common_str_arr.index(result_str)
                                except ValueError:
                                    if len(result_str) >= largest_str:
                                        common_str = result_str
                                        for seq_idx_i in range(0, input_range):
                                            if result_str not in input_dict[input_keys[seq_idx_i]]:
                                                common_str = None
                                                break
                                        if common_str is not None:
                                            try:
                                                common_str_arr.index(common_str)
                                            except ValueError:
                                                common_str_arr.append(common_str)
                                                for seq_str in common_str_arr:
                                                    seq_str_len = len(seq_str)
                                                    if seq_str_len > largest_str:
                                                        largest_str = seq_str_len

        common_str_arr_dup = list(common_str_arr)
        for seq_str in common_str_arr_dup:
            seq_str_len = len(seq_str)
            if seq_str_len < largest_str:
                common_str_arr.remove(seq_str)

        result_str = self.format_txt.list_to_string_delim_linebreak(common_str_arr)
        self.write_content_to_file(file_name, result_str)
        self.print_current_time("Completed parsing matches ")
        print(result_str)

    def independent_alleles_cat_example(self, file_name):
        file_content = self.load_and_return_content(file_name)
        pop_list = file_content[0].split(" ")
        gen_count_k = int(pop_list[0])
        min_expected_type = int(pop_list[1])
        default_parent = ["ss", "BB"]
        geno_type_list = [[["SS", "bb"], ["ss", "BB"]]]
        all_offspring = []
        for parent_pair in geno_type_list:
            p_one_unq_gtyp = self.bio_class.create_independent_allele_offspring_array(parent_pair[0])
            p_two_unq_gtyp = self.bio_class.create_independent_allele_offspring_array(parent_pair[1])
            for one_gt in p_one_unq_gtyp:
                for two_gt in p_two_unq_gtyp:
                    try:
                        all_offspring.index([one_gt, two_gt])
                    except ValueError:
                        all_offspring.append([one_gt, two_gt])

        all_offspring_dup = list(all_offspring)
        for child_type in all_offspring_dup:
            temp_list = list(str(child_type[0]) + str(child_type[1]))
            temp_list_dup = list(temp_list)
            temp_gtyp_arr = []
            temp_list_rng = len(temp_list)
            for idx_i in range(0, temp_list_rng):
                for idx_i_i in range(0, temp_list_rng):
                    if str(temp_list_dup[idx_i]).lower() == str(temp_list_dup[idx_i_i]).lower():
                        if idx_i != idx_i_i:
                            if temp_list_dup[idx_i] in temp_list and temp_list_dup[idx_i_i] in temp_list:
                                temp_list.remove(temp_list_dup[idx_i])
                                temp_list.remove(temp_list_dup[idx_i_i])
                                temp_gtyp = None
                                temp_gtyp = temp_list_dup[idx_i] + temp_list_dup[idx_i_i]
                                try:
                                    temp_gtyp_arr.index(temp_gtyp)
                                except ValueError:
                                    temp_gtyp_arr.append(temp_gtyp)
            all_offspring.remove(child_type)
            all_offspring.append(temp_gtyp_arr)
        print(all_offspring)
        geno_type_list = [[all_offspring[0]] * 2]
        all_offspring = []
        for parent_pair in geno_type_list:
            p_one_unq_gtyp = self.bio_class.create_independent_allele_offspring_array(parent_pair[0])
            p_two_unq_gtyp = self.bio_class.create_independent_allele_offspring_array(parent_pair[1])
            for one_gt in p_one_unq_gtyp:
                for two_gt in p_two_unq_gtyp:
                    try:
                        all_offspring.index([one_gt, two_gt])
                    except ValueError:
                        all_offspring.append([one_gt, two_gt])

        all_offspring_dup = list(all_offspring)
        for child_type in all_offspring_dup:
            temp_list = list(str(child_type[0]) + str(child_type[1]))
            temp_list_dup = list(temp_list)
            temp_gtyp_arr = []
            temp_list_rng = len(temp_list)
            for idx_i in range(0, temp_list_rng):
                for idx_i_i in range(0, temp_list_rng):
                    if str(temp_list_dup[idx_i]).lower() == str(temp_list_dup[idx_i_i]).lower():
                        if idx_i != idx_i_i:
                            if temp_list_dup[idx_i] in temp_list and temp_list_dup[idx_i_i] in temp_list:
                                temp_list.remove(temp_list_dup[idx_i])
                                temp_list.remove(temp_list_dup[idx_i_i])
                                temp_gtyp = None
                                temp_gtyp = temp_list_dup[idx_i] + temp_list_dup[idx_i_i]
                                try:
                                    temp_gtyp_arr.index(temp_gtyp)
                                except ValueError:
                                    temp_gtyp_arr.append(temp_gtyp)
            all_offspring.remove(child_type)
            all_offspring.append(temp_gtyp_arr)
        print(all_offspring)

    def independent_alleles(self, file_name):
        file_content = self.load_and_return_content(file_name)
        pop_list = file_content[0].split(" ")
        gen_count_k = int(pop_list[0])
        min_expected_offspring = int(pop_list[1])
        default_parent = ["Aa", "Bb"]
        all_offspring = [["Aa", "Bb"]]
        geno_type_list = all_offspring
        all_offspring = []
        for parent_pair in geno_type_list:
            p_one_unq_gtyp = self.bio_class.create_independent_allele_offspring_array(parent_pair)
            p_two_unq_gtyp = self.bio_class.create_independent_allele_offspring_array(default_parent)
            for one_gt in p_one_unq_gtyp:
                for two_gt in p_two_unq_gtyp:
                    try:
                        all_offspring.index([one_gt, two_gt])
                    except ValueError:
                        all_offspring.append([one_gt, two_gt])
        all_offspring_dup = list(all_offspring)
        for child_type in all_offspring_dup:
            temp_list = list(str(child_type[0]) + str(child_type[1]))
            temp_list_dup = list(temp_list)
            temp_gtyp_arr = []
            temp_list_rng = len(temp_list)
            for idx_i in range(0, temp_list_rng):
                for idx_i_i in range(0, temp_list_rng):
                    if str(temp_list_dup[idx_i]).lower() == str(temp_list_dup[idx_i_i]).lower():
                        if idx_i != idx_i_i:
                            if temp_list_dup[idx_i] in temp_list and temp_list_dup[idx_i_i] in temp_list:
                                temp_list.remove(temp_list_dup[idx_i])
                                temp_list.remove(temp_list_dup[idx_i_i])
                                try:
                                    temp_gtyp_arr.index(temp_list_dup[idx_i] + temp_list_dup[idx_i_i])
                                except ValueError:
                                    temp_gtyp_arr.append(temp_list_dup[idx_i] + temp_list_dup[idx_i_i])
            all_offspring.remove(child_type)
            all_offspring.append(temp_gtyp_arr)
        total_calculated_gt = 0
        for offspring in all_offspring:
            total_gt = offspring[0] + offspring[1]
            if total_gt.find('A') != -1 and \
                            total_gt.find('a') != -1 and \
                            total_gt.find('B') != -1 and \
                            total_gt.find('b') != -1:
                total_calculated_gt += 1
        success = total_calculated_gt / len(all_offspring)
        fail = 1 - success
        # (n choose k) * p^k * q^(n-k)
        # n choose k = n!/k!*(n-k)!
        # Each generation produce 2 records.
        # calculating total records generated.
        min_expected_type = 0
        growth_rate = 2
        total_offspring = growth_rate ** gen_count_k
        calculate_prop = 0
        while min_expected_type < min_expected_offspring:
            coeff_value_top = math.factorial(total_offspring)
            coeff_value_bottom = math.factorial(min_expected_type) * math.factorial(total_offspring - min_expected_type)
            n_choose_k = coeff_value_top / coeff_value_bottom
            final_result = n_choose_k * success ** min_expected_type * fail ** (total_offspring - min_expected_type)
            calculate_prop += final_result
            print("Total Expected -> | %s | Probability -> | %s | Calculated Total | %s | " % (
                min_expected_type, final_result, calculate_prop))
            min_expected_type += 1
        result_str = 1 - calculate_prop
        self.write_content_to_file(file_name, str(result_str))
        print(result_str)

    def finding_a_protein_motif(self, file_name):
        # Load Protein sequence files.
        output_folder_address = script_properties["resources"]["file_folder_address"]["roselind_problems"]
        output_folder_address += "protien_seq\\"
        file_content = self.load_and_return_content(file_name)
        content_map = {}
        for content in file_content:
            output_file_name = output_folder_address + content + ".fasta"
            collection_url = "http://www.uniprot.org/uniprot/" + content + ".fasta"
            if not os.path.exists(output_file_name):
                r = requests.get(collection_url)
                url_content = r.content.decode("utf-8")
                self.filefoldermanager.write_content_to_file(url_content, output_file_name, True)
            result_content = self.filefoldermanager.read_file_into_array_by_line(output_file_name)
            seq_str = ""
            for idx_i in range(1, len(result_content)):
                if len(result_content[idx_i]) > 0:
                    seq_str += result_content[idx_i]
            content_map[content] = seq_str

        # Collecting Amino Acid names.
        a_acid_arr = []
        for codon in script_properties["resources"]["protein_mapped"]:
            if len(script_properties["resources"]["protein_mapped"][codon]["one_letter_name"]) == 1:
                try:
                    a_acid_arr.index(script_properties["resources"]["protein_mapped"][codon]["one_letter_name"])
                except ValueError:
                    a_acid_arr.append(script_properties["resources"]["protein_mapped"][codon]["one_letter_name"])
        a_acid_arr = sorted(a_acid_arr)

        # Create Protein Motif sequence map.
        prot_motif = "N{P}[ST]{P}"
        prot_motif_arr = list(prot_motif)
        sort_motif_arr = []
        c_idx_i = 0
        motif_rng = len(prot_motif_arr)
        while c_idx_i < motif_rng:
            if prot_motif_arr[c_idx_i] == "{" or prot_motif_arr[c_idx_i] == "[":
                for c_idx_ix in range(c_idx_i, motif_rng):
                    if prot_motif_arr[c_idx_ix] == "}" or prot_motif_arr[c_idx_ix] == "]":
                        sort_motif_arr.append("".join(prot_motif_arr[c_idx_i:c_idx_ix + 1]))
                        c_idx_i = c_idx_ix + 1
                        break
            else:
                sort_motif_arr.append(prot_motif_arr[c_idx_i])
                c_idx_i += 1
        motif_rng = len(sort_motif_arr)
        final_option_map = {}
        for idx_ik in range(0, motif_rng):
            # when {X} is there means it is anything except X
            # when [XY] is there means either X or Y
            a_acd_opt = []
            if sort_motif_arr[idx_ik].find("[") != -1:
                a_acd_opt = list(sort_motif_arr[idx_ik].replace("[", "").replace("]", ""))
            elif sort_motif_arr[idx_ik].find("{") != -1:
                a_acd_all_dup = list(a_acid_arr)
                a_acd_opt = list(sort_motif_arr[idx_ik].replace("{", "").replace("}", ""))
                for a_acd in a_acd_opt:
                    a_acd_all_dup.remove(a_acd)
                a_acd_opt = a_acd_all_dup
            else:
                a_acd_opt = list(sort_motif_arr[idx_ik])
            final_option_map[str(idx_ik)] = list(a_acd_opt)

        # Create the amino acid List
        final_motif_seq = []
        for idx_ik in range(0, motif_rng):
            temp_arr = []
            a_acd_arr = final_option_map[str(idx_ik)]
            if idx_ik == 0:
                temp_arr = a_acd_arr
            else:
                for a_acd in a_acd_arr:
                    for prev_seq in final_motif_seq:
                        motif_str = prev_seq + a_acd
                        try:
                            temp_arr.index(motif_str)
                        except ValueError:
                            temp_arr.append(motif_str)
            final_motif_seq = temp_arr
        protein_names = list(content_map.keys())

        # Collect resulting locations.
        protein_seq_result = {}
        for k_key in protein_names:
            prot_seq = content_map[k_key]
            temp_arr = []
            for motif_seq in final_motif_seq:
                if prot_seq.find(motif_seq) != -1:
                    temp_arr.append(str(prot_seq.find(motif_seq) + 1))
            protein_seq_result[k_key] = temp_arr
        result_str = ""
        for k_key in protein_names:
            if len(protein_seq_result[k_key]) > 0:
                result_str += k_key + "\n"
                result_str += " ".join(protein_seq_result[k_key]) + "\n"
        self.write_content_to_file(file_name, str(result_str))
        print(result_str)

    def inferring_mrna_from_protein(self, file_name):

        # collecting data from file.
        file_content = self.load_and_return_content(file_name)
        seq_str = ""
        for idx_i in range(0, len(file_content)):
            if len(file_content[idx_i]) > 0:
                seq_str += file_content[idx_i]
        seq_arr = list(seq_str)

        # creating amino acid map.
        a_acid_map = {}
        for codon in script_properties["resources"]["protein_mapped"]:
            if len(script_properties["resources"]["protein_mapped"][codon]["one_letter_name"]) == 1:
                one_letter = script_properties["resources"]["protein_mapped"][codon]["one_letter_name"]
                try:
                    a_acid_map[one_letter]
                except KeyError:
                    a_acid_map[one_letter] = []
                a_acid_map[one_letter].append(codon)

        # Iterate protein Seq and count
        seq_arr_rng = len(seq_arr)
        total_seq_num = 1
        mod_count = 1000000
        for idx_i in range(0, seq_arr_rng):
            total_seq_num = (total_seq_num * len(a_acid_map[seq_arr[idx_i]])) % mod_count

        total_seq_num = (total_seq_num * 3) % mod_count
        result_str = str(total_seq_num)
        self.write_content_to_file(file_name, str(result_str))
        print(result_str)

    def open_reading_frames(self, file_name):
        content = self.load_and_return_content_delimted(file_name, ">")
        input_dict = {}
        for v_val in content:
            value_arr = v_val.split("\n")
            input_dict[value_arr[0]] = "".join([itm for itm in value_arr if len(itm) > 0 and itm != value_arr[0]])

        input_keys = list(input_dict.keys())
        comb_dict = {}
        for k_key in input_keys:
            comb_dict[k_key] = {}
            comb_dict[k_key]["orig"] = input_dict[k_key]
            comb_dict[k_key]["orig_mrna"] = self.bio_class.convert_to_rna(comb_dict[k_key]["orig"])
            comb_dict[k_key]["rev_comp"] = self.bio_class.create_complementing_strand_of_dna(comb_dict[k_key]["orig"])
            comb_dict[k_key]["rev_comp_mrna"] = self.bio_class.convert_to_rna(comb_dict[k_key]["rev_comp"])

        start_codons_str = ""
        stop_codons_str = ""
        stop_codons = []
        for codon in script_properties["resources"]["protein_mapped"]:
            if script_properties["resources"]["protein_mapped"][codon]["complete_name"] == "STOP":
                stop_codons_str += "(%s)|" % codon
                stop_codons.append(codon)
            if script_properties["resources"]["protein_mapped"][codon]["complete_name"] == "Methionine":
                start_codons_str += "(%s)|" % codon
        start_codons_str = start_codons_str[0:start_codons_str.rindex("|")]
        stop_codons_str = stop_codons_str[0:stop_codons_str.rindex("|")]
        start_matcher = re.compile(start_codons_str)
        stop_matcher = re.compile(stop_codons_str)

        matcher_list = {}
        for k_key in input_keys:
            sub_key_list = list(comb_dict[k_key].keys())
            matcher_list[k_key] = {}
            for kk_key in sub_key_list:
                if kk_key.find("mrna") != -1:
                    matcher_list[k_key][kk_key] = {}
                    start_matches = start_matcher.finditer(comb_dict[k_key][kk_key])
                    stop_matches = stop_matcher.finditer(comb_dict[k_key][kk_key])
                    matcher_list[k_key][kk_key]["start_match"] = []
                    for match in start_matches:
                        matcher_list[k_key][kk_key]["start_match"].append(match.span())
                    matcher_list[k_key][kk_key]["stop_match"] = []
                    for match in stop_matches:
                        matcher_list[k_key][kk_key]["stop_match"].append(match.span())
        result_arr = []
        for k_key in input_keys:
            for kk_key in matcher_list[k_key].keys():
                start_arr = matcher_list[k_key][kk_key]["start_match"]
                stop_arr = matcher_list[k_key][kk_key]["stop_match"]
                for str_tup in start_arr:
                    for stp_tup in stop_arr:
                        if str_tup[0] < stp_tup[0] and (stp_tup[1] - str_tup[0]) % 3 == 0:
                            sequence = comb_dict[k_key][kk_key][str_tup[0]:stp_tup[0]]
                            char_count = 3
                            codon_list = [sequence[i:i + char_count] for i in range(0, len(sequence), char_count)]
                            usable = True
                            for stp_codon in stop_codons:
                                if stp_codon in codon_list:
                                    usable = False
                                    break
                            if usable:
                                protein = self.bio_class.rna_to_protein(comb_dict[k_key][kk_key][str_tup[0]:stp_tup[1]])
                                try:
                                    result_arr.index(protein)
                                except ValueError:
                                    result_arr.append(protein)
        result_str = self.format_txt.list_to_string_delim_linebreak(result_arr)
        self.write_content_to_file(file_name, str(result_str))
        print(result_str)

    def enumerating_gene_orders(self, file_name):
        file_content = self.load_and_return_content(file_name)
        p_val = int(file_content[0]) + 1
        temp_val = "_int "
        init_str = ""
        end_str = ""
        if_statement = ""
        for i_val in range(1, p_val):
            init_str += " str(" + "i" * i_val + temp_val + "), "
            end_str += " for " + "i" * i_val + temp_val + " in range(1, p_val) "
            if_statement_temp = ""
            for x_val in range(1, p_val):
                if x_val != i_val:
                    if_statement_temp += " if " + "i" * i_val + temp_val + " != " + "i" * x_val + temp_val
            if_statement += if_statement_temp
        init_str = init_str[0:init_str.rindex(",")]
        init_str = "(" + init_str + ")"
        out_val_orig = []
        code_run = "out_val = [" + init_str + end_str + if_statement + "]"
        exec(code_run, locals())
        out_val_orig = list(locals()['out_val'])
        locals()['out_val'] = ""
        result_arr = [str(len(out_val_orig))]
        for tup_itm in out_val_orig:
            result_arr.append(" ".join(tup_itm))
        result_str = self.format_txt.list_to_string_delim_linebreak(result_arr)
        self.write_content_to_file(file_name, str(result_str))
        print(result_str)

    def calculating_protein_mass(self, file_name):
        # collecting data from file.
        file_content = self.load_and_return_content(file_name)
        seq_str = ""
        for idx_i in range(0, len(file_content)):
            if len(file_content[idx_i]) > 0:
                seq_str += file_content[idx_i]
        seq_arr = list(seq_str)
        protein_mapped = script_properties["resources"]["protein_mapped"]
        protein_weight = {}
        for codon in protein_mapped:
            if len(protein_mapped[codon]["one_letter_name"]) == 1:
                protein_weight[protein_mapped[codon]["one_letter_name"]] = protein_mapped[codon]["weight"]
        prot_mass = 0
        for idx_i in range(0, len(seq_arr)):
            prot_mass += float(protein_weight[seq_arr[idx_i]])
        result_str = str(prot_mass)
        self.write_content_to_file(file_name, str(result_str))
        print(result_str)

    def locating_restriction_sites(self, file_name):
        self.print_current_time("Started Processing.")
        temp_result_arr = []
        content = self.load_and_return_content_delimted(file_name, ">")
        input_dict = {}
        for v_val in content:
            value_arr = v_val.split("\n")
            input_dict[value_arr[0]] = "".join([itm for itm in value_arr if len(itm) > 0 and itm != value_arr[0]])
        core_dict = {}
        input_keys = sorted(list(input_dict.keys()))
        for k_key in input_keys:
            core_dict[k_key] = list(input_dict[k_key])
        input_range = len(input_keys)
        for idx_i in range(0, input_range):
            seq_one_range = len(core_dict[input_keys[idx_i]])
            for match_size in range(4, 13):
                for nuc_x in range(0, seq_one_range):
                    comp_tup_1 = "".join(core_dict[input_keys[idx_i]][nuc_x:nuc_x + match_size])
                    comp_tup_2 = self.bio_class.create_reverse_complementing_strand_of_dna(comp_tup_1)
                    if comp_tup_1 == comp_tup_2 and len(comp_tup_1) == match_size:
                        temp_result_arr.append([nuc_x + 1, match_size])
        result_str = ""
        for idx_i in range(0, len(temp_result_arr)):
            result_str += "%s %s\n" % (temp_result_arr[idx_i][0], temp_result_arr[idx_i][1])
        self.write_content_to_file(file_name, str(result_str))
        print(result_str)

    def rna_splicing(self, file_name):
        temp_result_arr = []
        content = self.load_and_return_content_delimted(file_name, ">")
        input_dict = {}
        seq_str = content[0].split("\n")
        complete_seq = "".join(seq_str[1: len(seq_str)])
        intron_arr = []
        for idx_i in range(1, len(content)):
            seq_str = content[idx_i].split("\n")
            intron_arr.append("".join(seq_str[1: len(seq_str)]))
        for idx_i in range(0, len(intron_arr)):
            complete_seq = complete_seq.replace(intron_arr[idx_i], "")
        rna_seq = self.bio_class.convert_to_rna(complete_seq)
        result_str = self.bio_class.rna_to_protein(rna_seq)
        self.write_content_to_file(file_name, str(result_str))
        print(result_str)
        # print(intron_arr)

    def enumerating_k_mers_lexicographically(self, file_name):
        file_content = self.load_and_return_content(file_name)
        letter_list = list(file_content[0].split(" "))
        pmu_len = int(file_content[1])
        result_arr = []
        letter_list_len = len(letter_list)
        temp_val = "_int "
        init_str = ""
        end_str = ""
        for i_val in range(1, pmu_len + 1):
            init_str += "i" * i_val + temp_val + ", "
            end_str += " for " + "i" * i_val + temp_val + " in range(0, letter_list_len) "
        init_str = init_str[0:init_str.rindex(",")]
        init_str = "(" + init_str + ")"
        code_run = "out_val = [" + init_str + end_str + "]"
        exec(code_run, locals())
        out_val_orig = list(locals()['out_val'])
        for tup_item in out_val_orig:
            aplh_len = len(tup_item)
            tmp_rst = ""
            for alp_i in range(0, aplh_len):
                tmp_rst += letter_list[tup_item[alp_i]]
            result_arr.append(tmp_rst)
        result_str = self.format_txt.list_to_string_delim_linebreak(result_arr)
        self.write_content_to_file(file_name, str(result_str))
        print(result_str)

    def longest_increasing_subsequence(self, file_name):
        file_content = self.load_and_return_content(file_name)
        complete_seq = list(file_content[1].split(" "))
        complete_seq = [int(itm_val) for itm_val in complete_seq]
        seq_list = [complete_seq, list(reversed(complete_seq))]
        pre_result_arr = []
        for seq in seq_list:
            M = [None] * len(seq)
            P = [None] * len(seq)
            L = 1
            M[0] = 0
            for i in range(1, len(seq)):
                lower = 0
                upper = L
                if seq[M[upper - 1]] < seq[i]:
                    j = upper
                else:
                    while upper - lower > 1:
                        mid = (upper + lower) // 2
                        if seq[M[mid - 1]] < seq[i]:
                            lower = mid
                        else:
                            upper = mid
                    j = lower
                P[i] = M[j - 1]
                if j == L or seq[i] < seq[M[j]]:
                    M[j] = i
                    L = max(L, j + 1)
            result = []
            pos = M[L - 1]
            for _ in range(L):
                result.append(seq[pos])
                pos = P[pos]
            pre_result_arr.append(result)
        pre_result_arr[0] = list(reversed(pre_result_arr[0]))
        result_arr = [" ".join([str(x) for x in tmp_arr]) for tmp_arr in pre_result_arr]
        result_str = self.format_txt.list_to_string_delim_linebreak(result_arr)
        self.write_content_to_file(file_name, str(result_str))
        print(result_str)

    def genome_assembly_as_shortest_superstring(self, file_name):
        self.print_current_time("Started Processing.")
        result_arr = []
        content = self.load_and_return_content_delimted(file_name, ">")
        input_dict = {}
        for v_val in content:
            value_arr = v_val.split("\n")
            input_dict[value_arr[0]] = "".join([itm for itm in value_arr if len(itm) > 0 and itm != value_arr[0]])
        core_dict = {}
        input_keys = sorted(list(input_dict.keys()))
        for k_key in input_keys:
            core_dict[k_key] = list(input_dict[k_key])
        input_range = len(input_keys)
        match_list = []
        zero_list = []
        for idx_i in range(0, input_range):
            seq_one_range = len(core_dict[input_keys[idx_i]])
            for idx_i_i in range(idx_i + 1, input_range):
                if idx_i != idx_i_i:
                    seq_two_range = len(core_dict[input_keys[idx_i_i]])
                    if seq_two_range % 2 == 0:
                        cur_rng = seq_two_range / 2
                    else:
                        cur_rng = (seq_two_range + (seq_two_range % 2)) / 2
                    split_seq = [input_dict[input_keys[idx_i_i]][0:int(cur_rng)],
                                 input_dict[input_keys[idx_i_i]][int(cur_rng):seq_two_range]]
                    for seq in split_seq:
                        str_idx = input_dict[input_keys[idx_i]].find(seq)
                        if str_idx != -1:
                            match_idx = input_dict[input_keys[idx_i_i]].find(seq)
                            # finding largest match
                            match_size = len(seq)
                            if str_idx != 0 and match_idx != 0:
                                while core_dict[input_keys[idx_i]][str_idx - 1:(str_idx - 1) + match_size + 1] == \
                                        core_dict[input_keys[idx_i_i]][match_idx - 1:(match_idx - 1) + match_size + 1]:
                                    str_idx -= 1
                                    match_idx -= 1
                                    match_size += 1
                            while core_dict[input_keys[idx_i]][str_idx:str_idx + match_size + 1] == \
                                    core_dict[input_keys[idx_i_i]][match_idx:match_idx + match_size + 1]:
                                match_size += 1
                            if match_idx == 0:
                                zero_list.append(input_keys[idx_i_i])
                                match_list.append(
                                    [[input_keys[idx_i_i], match_idx], [input_keys[idx_i], str_idx], match_size])
                            elif str_idx == 0:
                                zero_list.append(input_keys[idx_i])
                                match_list.append(
                                    [[input_keys[idx_i], str_idx], [input_keys[idx_i_i], match_idx], match_size])
                            else:
                                match_list.append(
                                    [[input_keys[idx_i], str_idx], [input_keys[idx_i_i], match_idx + 1], match_size])
        fst_sq_name = ""
        for seq_name in input_keys:
            if seq_name not in zero_list:
                fst_sq_name = seq_name
                break
        sorted_match_list = []
        match_list_dup = list(match_list)
        c_sq_name = fst_sq_name
        match_list_len = len(match_list_dup)
        while len(match_list_dup) > 0:
            for idx_i in range(0, match_list_len):
                if match_list_dup[idx_i][1][0] == c_sq_name:
                    sorted_match_list.append(match_list_dup[idx_i])
                    c_sq_name = match_list_dup[idx_i][0][0]
                    match_list_dup.remove(match_list_dup[idx_i])
                    break
            match_list_len = len(match_list_dup)
        if len(sorted_match_list) == len(match_list):
            print("ALL MATCHES ACCOUNTED FOR. ")

        match_list_len = len(sorted_match_list)
        paired_seq = []
        for idx_i in range(0, match_list_len):
            match = sorted_match_list[idx_i]
            temp_str = "".join(core_dict[match[1][0]][0:match[1][1]])
            temp_str += "".join(core_dict[match[0][0]][0:len(core_dict[match[0][0]])])
            paired_seq.append(temp_str)
        super_str = ""
        for idx_i in range(0, len(paired_seq)):
            if idx_i == 0:
                super_str = paired_seq[idx_i]
            else:
                c_seq_len = len(paired_seq[idx_i])
                str_pnt = 0
                for nuc_x in range(0, c_seq_len):
                    if super_str.find(paired_seq[idx_i][0: c_seq_len - nuc_x]) != -1:
                        str_pnt = c_seq_len - nuc_x
                        break
                super_str += paired_seq[idx_i][str_pnt:c_seq_len]
        result_str = super_str
        self.write_content_to_file(file_name, result_str)
        print(result_str)

    def create_class_name_items(self, problem_name, file_name):
        input_file_address = script_properties["resources"]["file_folder_address"]["roselind_problems"]
        input_file_address += "rosalind_" + file_name + ".txt"
        content = ""
        self.filefoldermanager.write_content_to_file(content, input_file_address, True)
        print(input_file_address)
        print("    def " + problem_name.replace(" ", "_").lower() + "(self, file_name):")
        print("        self.print_current_time(\"Started Processing.\")")
        print("    main_class." + problem_name.replace(" ",
                                                       "_").lower() + "(" + "\"rosalind_" + file_name + ".txt\"" + ")")

    def fibonacci_numbers(self, file_name):
        self.print_current_time("Started Processing.")
        file_content = self.load_and_return_content(file_name)
        c_num = int(file_content[0])
        f_arr = [None] * (c_num + 1)
        f_arr_len = len(f_arr)
        for idx_i in range(0, f_arr_len):
            if idx_i == 0:
                f_arr[idx_i] = 0
            elif idx_i == 1:
                f_arr[idx_i] = 1
            elif idx_i > 1:
                f_arr[idx_i] = f_arr[idx_i - 1] + f_arr[idx_i - 2]
        # while f_arr[len(f_arr)-1] < c_num:
        #     if f_arr[len(f_arr)-1] == 0:
        #         f_arr.append(1)
        #     else:
        #         f_arr.append(f_arr[len(f_arr)-1] + f_arr[len(f_arr)-2])
        #     print(f_arr)
        #
        result_str = str(f_arr[len(f_arr) - 1])
        self.write_content_to_file(file_name, result_str)
        print(result_str)

    def return_index_arr(self, k_val, n_list, lookup_rng):
        diff = int((lookup_rng[1] - lookup_rng[0]) / 2)
        if diff % 2 != 0:
            diff = ((lookup_rng[1] + 1) - lookup_rng[0]) / 2
        new_idx = int(lookup_rng[0] + diff)
        if n_list[new_idx] == k_val:
            lookup_rng = (int(new_idx), int(new_idx))
        elif n_list[new_idx] > k_val:
            lookup_rng = (int(lookup_rng[0]), int(new_idx))
        elif n_list[new_idx] < k_val:
            lookup_rng = (int(new_idx), int(lookup_rng[1]))
        return lookup_rng

    def binary_search(self, file_name):
        self.print_current_time("Started Processing.")
        file_content = self.load_and_return_content(file_name)
        total_n = int(file_content[0])
        total_m = int(file_content[1])
        n_list = [int(x) for x in file_content[2].split(" ") if len(x) > 0]
        k_list = [int(x) for x in file_content[3].split(" ") if len(x) > 0]
        k_list_len = len(k_list)
        result_arr = [None] * k_list_len
        for k_val in range(0, k_list_len):
            lookup_rng = (0, len(n_list))
            while lookup_rng[1] - lookup_rng[0] > 2:
                lookup_rng = self.return_index_arr(k_list[k_val], n_list, lookup_rng)
            final_idx = -1
            if lookup_rng[0] == lookup_rng[1]:
                final_idx = lookup_rng[0]
            else:
                for idx_i in range(lookup_rng[0], lookup_rng[1]):
                    if n_list[idx_i] == k_list[k_val]:
                        final_idx = idx_i
            if final_idx != -1:
                final_idx += 1
            result_arr[k_val] = str(final_idx)
        result_str = " ".join(result_arr)
        self.write_content_to_file(file_name, str(result_str))
        print(result_str)

    def degree_array(self, file_name):
        self.print_current_time("Started Processing.")
        file_content = self.load_and_return_content(file_name)
        line_count = len(file_content)
        n_ver = file_content[0].split(" ")[0]
        m_edg_count = file_content[0].split(" ")[1]
        node_list = [int(edg_val) for line_num in range(1, line_count) if len(file_content[line_num]) > 0 for edg_val in
                     file_content[line_num].split(" ")]
        unq_node_list = []
        node_list_len = len(node_list)
        for idx_i in range(0, node_list_len):
            try:
                unq_node_list.index(node_list[idx_i])
            except ValueError:
                unq_node_list.append(node_list[idx_i])
        unq_node_list_len = len(unq_node_list)
        for idx_i in range(0, unq_node_list_len):
            for idx_i_i in range(idx_i, unq_node_list_len):
                if unq_node_list[idx_i] > unq_node_list[idx_i_i]:
                    unq_node_list[idx_i], unq_node_list[idx_i_i] = unq_node_list[idx_i_i], unq_node_list[idx_i]
        result_arr = [0] * unq_node_list_len
        for idx_i in range(0, unq_node_list_len):
            result_arr[idx_i] = len(
                [idx_k for idx_k in range(0, node_list_len) if node_list[idx_k] == unq_node_list[idx_i]])
        result_str = " ".join([str(result_arr[idx_i]) for idx_i in range(0, len(result_arr))])
        self.write_content_to_file(file_name, str(result_str))
        print(result_str)
        print(json.dumps(result_arr))

    def insertion_sort(self, file_name):
        self.print_current_time("Started Processing.")
        file_content = self.load_and_return_content(file_name)
        n_ver = int(file_content[0])
        num_list = [int(num_val) for num_val in file_content[1].split(" ")]
        swp_count = 0
        for i in range(1, n_ver):
            k = i
            while k > 0 and num_list[k] < num_list[k - 1]:
                num_list[k - 1], num_list[k] = num_list[k], num_list[k - 1]
                k -= 1
                swp_count += 1
        result_str = str(swp_count)
        self.write_content_to_file(file_name, str(result_str))
        print(result_str)

    def doubledegree_array(self, file_name):
        self.print_current_time("Started Processing.")
        file_content = self.load_and_return_content(file_name)
        line_count = len(file_content)
        n_ver = int(file_content[0].split(" ")[0])
        m_edg_count = int(file_content[0].split(" ")[1])
        node_list = [int(edg_val) for line_num in range(1, line_count) if len(file_content[line_num]) > 0 for edg_val in
                     file_content[line_num].split(" ")]
        paird_node_list = [file_content[line_num].split(" ") for line_num in range(1, line_count) if
                           len(file_content[line_num]) > 0]
        unq_node_list = []
        node_list_len = len(node_list)
        for idx_i in range(0, node_list_len):
            try:
                unq_node_list.index(node_list[idx_i])
            except ValueError:
                unq_node_list.append(node_list[idx_i])
        unq_node_list_len = len(unq_node_list)
        for idx_i in range(0, unq_node_list_len):
            for idx_i_i in range(idx_i, unq_node_list_len):
                if unq_node_list[idx_i] > unq_node_list[idx_i_i]:
                    unq_node_list[idx_i], unq_node_list[idx_i_i] = unq_node_list[idx_i_i], unq_node_list[idx_i]
        deg_count = [0] * n_ver
        for idx_i in range(0, unq_node_list_len):
            deg_count[idx_i] = len(
                [idx_k for idx_k in range(0, node_list_len) if node_list[idx_k] == unq_node_list[idx_i]])
        result_arr = [0] * n_ver
        nbr_lists = [[] for x in range(0, (n_ver + 1))]
        for pair in paird_node_list:
            try:
                nbr_lists[int(pair[0])].index(int(pair[1]))
            except ValueError:
                nbr_lists[int(pair[0])].append(int(pair[1]))
            try:
                nbr_lists[int(pair[1])].index(int(pair[0]))
            except ValueError:
                nbr_lists[int(pair[1])].append(int(pair[0]))

        for idx_i in range(0, unq_node_list_len):
            for idx_nbr in range(0, len(nbr_lists[unq_node_list[idx_i]])):
                result_arr[idx_i] += deg_count[nbr_lists[unq_node_list[idx_i]][idx_nbr] - 1]
        result_str = " ".join([str(result_arr[idx_i]) for idx_i in range(0, len(result_arr))])
        self.write_content_to_file(file_name, str(result_str))
        print(result_str)

    def return_sorted_unique_list(self, master_list):
        unique_list = []
        for idx_i in range(0, len(master_list)):
            try:
                unique_list.index(master_list[idx_i])
            except ValueError:
                unique_list.append(master_list[idx_i])
        for i in range(1, len(unique_list)):
            k = i
            while k > 0 and unique_list[k] < unique_list[k - 1]:
                unique_list[k - 1], unique_list[k] = unique_list[k], unique_list[k - 1]
                k -= 1
        return unique_list

    def return_value_count(self, unq_list, master_list):
        unq_lst_len = len(unq_list)
        val_cnt_lst = [0] * unq_lst_len
        for idx_i in range(0, unq_lst_len):
            val_cnt_lst[idx_i] = len([x for x in range(0, len(master_list)) if master_list[x] == unq_list[idx_i]])
        return val_cnt_lst

    def majority_element(self, file_name):
        self.print_current_time("Started Processing.")
        file_content = self.load_and_return_content(file_name)
        line_count = len(file_content)
        list_size = int(file_content[0].split(" ")[0])
        list_length = int(file_content[0].split(" ")[1])
        list_all = [[int(x) for x in file_content[line_num].split(" ")] for line_num in range(1, line_count) if
                    len(file_content[line_num]) > 0]
        list_all_len = len(list_all)
        half_val = list_length / 2
        if half_val % 2 != 0:
            half_val = (list_length + 1) / 2
        half_val += 1
        result_arr = [-1] * list_all_len
        for list_i in range(0, list_all_len):
            unq_list = self.return_sorted_unique_list(list_all[list_i])
            val_count_list = self.return_value_count(unq_list, list_all[list_i])
            for val_cnt in range(0, len(val_count_list)):
                if val_count_list[val_cnt] >= half_val:
                    result_arr[list_i] = unq_list[val_cnt]
        result_str = " ".join([str(result_arr[idx_i]) for idx_i in range(0, len(result_arr))])
        self.write_content_to_file(file_name, str(result_str))
        print(result_str)

    def merge(self, left, right):
        result = []
        left_idx, right_idx = 0, 0
        while left_idx < len(left) and right_idx < len(right):
            if left[left_idx] <= right[right_idx]:
                result.append(left[left_idx])
                left_idx += 1
            else:
                result.append(right[right_idx])
                right_idx += 1
        if left:
            result.extend(left[left_idx:])
        if right:
            result.extend(right[right_idx:])
        return result

    def merge_two_sorted_arrays(self, file_name):
        self.print_current_time("Started Processing.")
        file_content = self.load_and_return_content(file_name)
        lst_one_size = int(file_content[0])
        list_one = [int(x) for x in file_content[1].split(" ")]
        lst_two_size = int(file_content[2])
        list_two = [int(x) for x in file_content[3].split(" ")]
        result_arr = self.merge(list_two, list_one)
        result_str = " ".join([str(result_arr[idx_i]) for idx_i in range(0, len(result_arr))])
        self.write_content_to_file(file_name, str(result_str))
        print(result_str)

    def sort_tuple(self, num_list):
        n_ver = len(num_list)
        for i in range(1, n_ver):
            k = i
            while k > 0 and num_list[k][0] < num_list[k - 1][0]:
                num_list[k - 1], num_list[k] = num_list[k], num_list[k - 1]
                k -= 1
        return num_list

    def sort_list(self, num_list):
        n_ver = len(num_list)
        for i in range(1, n_ver):
            k = i
            while k > 0 and num_list[k] < num_list[k - 1]:
                num_list[k - 1], num_list[k] = num_list[k], num_list[k - 1]
                k -= 1
        return num_list

    def test_sorting(self, file_name):
        self.print_current_time("Started Processing.")
        file_content = self.load_and_return_content(file_name)
        sort_list = [int(x) for x in file_content[0].split(" ")]
        sort_list_len = len(sort_list)
        self.print_current_time("Started Processing.")
        temp_arr = sort_list
        section_val = 3
        for x in range(0, 5):
            section_val += x
            total_rounds = int(sort_list_len / section_val) + 1
            temp_arr_one = temp_arr
            add_val_list = [0] * total_rounds
            idx_i = 0
            for round_i in range(0, total_rounds):
                add_val_list[round_i] = (sum(temp_arr_one[idx_i:(idx_i + section_val)]), idx_i)
                idx_i += section_val
            add_val_list = self.sort_tuple(add_val_list)
            temp_arr = [] * sort_list_len
            idx_k = 0
            for idx_i in range(0, len(add_val_list)):
                sorted_arr = self.sort_list(temp_arr_one[add_val_list[idx_i][1]: add_val_list[idx_i][1] + section_val])
                temp_arr[idx_i:(idx_i + section_val)] = sorted_arr
                idx_k += section_val
        self.print_current_time("Started Processing.")
        print(temp_arr)
        print(sort_list)
        # str_temp = " %s | %s | %s | " % (add_val_list[idx_i], sum(sort_list[add_val_list[idx_i][1]:(add_val_list[idx_i][1] + section_val)], [add_val_list[idx_i][1], add_val_list[idx_i][1] + section_val]))
        # if add_val_list[idx_i][1] == 0:
        #     print(sort_list[add_val_list[idx_i][1]: add_val_list[idx_i][1] + section_val])
        # if add_val_list[idx_i][1] == 5:
        #     print(sort_list[add_val_list[idx_i][1]: add_val_list[idx_i][1] + section_val])
        #     print(sort_list[0: 10])
        # t_val = ((round_i + 1) * section_val + round_i) + 1
        # for round_i in range(0, total_rounds):
        #     # print(" %s || %s " % (add_val_list[round_i], sum(sort_list[idx_i:(idx_i + section_val)])))
        #     idx_i += (section_val + 1)
        #
        #     print(" %s || %s " % (idx_i, t_val))
        #
        # self.print_current_time("Started Processing.")
        #
        #     # print(round_i)
        # print(idx_count)
        # print(len(sort_list))
        # result_arr = []
        # result_str = " ".join([str(result_arr[idx_i]) for idx_i in range(0, len(result_arr))])
        # self.write_content_to_file(file_name, str(result_str))
        # print(result_str)

    def algorithm_2sum(self, file_name):

        self.print_current_time("Started Reading File.")
        file_content = self.load_and_return_content(file_name)
        self.print_current_time("Completed Reading File.")

        self.print_current_time("Started parsing File.")
        k_arr_count = int(file_content[0].split(" ")[0])
        arr_len_count = int(file_content[0].split(" ")[1])
        half_pnt = int(arr_len_count / 2)
        if half_pnt % 2 != 0:
            half_pnt = int((arr_len_count + 1) / 2)
        rs_list = [None] * k_arr_count
        idx_count = arr_len_count - 1
        rs_list_unsorted = [None] * k_arr_count
        for idx_i in range(0, k_arr_count):
            rs_list_unsorted[idx_i] = [int(x) for x in file_content[idx_i + 1].split(" ")]
            rs_list[idx_i] = list(sorted(rs_list_unsorted[idx_i]))
        self.print_current_time("Completed parsing and preparing order id request.")
        self.print_current_time("Collecting Unique Indexes.")
        small_arr = [0] * k_arr_count
        for idx_i in range(0, k_arr_count):
            c_idx = half_pnt
            if rs_list[idx_i][c_idx] >= 0:
                while rs_list[idx_i][c_idx - 1] >= 0:
                    c_idx -= 1
                    # c_idx -= 1
            elif rs_list[idx_i][c_idx] <= 0:
                while rs_list[idx_i][c_idx + 1] < 0:
                    c_idx += 1
                c_idx += 1
            small_arr[idx_i] = c_idx
        final_idx_arr = [[] for x in range(0, k_arr_count)]
        for idx_i in range(0, k_arr_count):
            tmp_idx_tup = [small_arr[idx_i], small_arr[idx_i] + 1]
            while tmp_idx_tup[0] >= 0 and tmp_idx_tup[1] <= idx_count:
                if abs(rs_list[idx_i][tmp_idx_tup[0]]) > rs_list[idx_i][tmp_idx_tup[1]]:
                    tmp_idx_tup[1] += 1
                elif abs(rs_list[idx_i][tmp_idx_tup[0]]) < rs_list[idx_i][tmp_idx_tup[1]]:
                    tmp_idx_tup[0] -= 1
                elif abs(rs_list[idx_i][tmp_idx_tup[0]]) == rs_list[idx_i][tmp_idx_tup[1]]:
                    try:
                        final_idx_arr[idx_i].index(rs_list[idx_i][tmp_idx_tup[1]])
                    except ValueError:
                        final_idx_arr[idx_i].append(rs_list[idx_i][tmp_idx_tup[1]])
                    tmp_idx_tup[1] += 1
                    tmp_idx_tup[0] -= 1
        self.print_current_time("Unique Index Collection Complete.")
        self.print_current_time("Started Processing Each Index ")
        result_arr = [None] * k_arr_count
        for idx_i in range(0, k_arr_count):
            fnl_lst_rng = len(final_idx_arr[idx_i])
            for e_idx_x in range(0, fnl_lst_rng):
                tmp_p_list = []
                tmp_p = 0
                idx_val = 0
                while idx_val != -1:
                    try:
                        tmp_loc = rs_list_unsorted[idx_i][idx_val:].index(final_idx_arr[idx_i][e_idx_x])
                        tmp_p_list.append(tmp_loc + idx_val)
                        idx_val += tmp_loc + 1
                        # print(" %s | %s | %s " % (idx_i, rs_list[idx_i][idx_k], idx_val))
                    except ValueError:
                        idx_val = -1
                for idx_tmp in range(0, len(tmp_p_list)):
                    try:
                        minus_idx = rs_list_unsorted[idx_i][tmp_p_list[idx_tmp] + 1:].index(
                            -final_idx_arr[idx_i][e_idx_x])
                        minus_idx += tmp_p_list[idx_tmp] + 1
                    except ValueError:
                        minus_idx = -1
                    if minus_idx != -1:
                        result_arr[idx_i] = [tmp_p_list[idx_tmp] + 1, minus_idx + 1]
                        break
                if result_arr[idx_i] is not None:
                    break
        self.print_current_time("Completed Processing all Indexes ")
        result_str = ""
        for idx_i in range(0, len(result_arr)):
            temp_str = "-1\n"
            if result_arr[idx_i] is not None:
                temp_tpl = result_arr[idx_i]
                if temp_tpl[0] < temp_tpl[1]:
                    temp_str = "%s %s\n" % (temp_tpl[0], temp_tpl[1])
                else:
                    temp_str = "%s %s\n" % (temp_tpl[1], temp_tpl[0])
            result_str += temp_str
        for idx_i in range(0, len(result_arr)):
            if result_arr[idx_i] is not None:
                print(" %s | %s " % (
                    rs_list_unsorted[idx_i][result_arr[idx_i][0] - 1],
                    rs_list_unsorted[idx_i][result_arr[idx_i][1] - 1]))
        self.write_content_to_file(file_name, str(result_str))
        print(result_str)

    def breadth_first_search(self, file_name):
        file_content = self.load_and_return_content(file_name)
        line_count = len(file_content)
        n_ver = int(file_content[0].split(" ")[0]) + 1
        m_edg_count = int(file_content[0].split(" ")[1])
        paird_node_list = [[int(x) for x in file_content[line_num].split(" ")] for line_num in range(1, line_count) if
                           len(file_content[line_num]) > 0]
        hr_map = [[] * n_ver for x in range(0, n_ver)]

        self.print_current_time("Started Processing Matrix.")
        for idx_i in range(0, m_edg_count):
            if paird_node_list[idx_i][1] > 1:
                hr_map[paird_node_list[idx_i][0]].append(paird_node_list[idx_i][1])
        self.print_current_time("Completed Processing Matrix.")

        hr_dist_map = [[] * n_ver for x in range(0, n_ver + 1)]
        hr_dist_map[1] = list(sorted(hr_map[1]))
        hr_collected_map = [x for x in hr_dist_map[1]]

        self.print_current_time("Started Creating Map.")
        for idx_i in range(1, n_ver):
            tmp_map = []
            for d_list in range(0, len(hr_dist_map[idx_i])):
                for pnt_m in range(0, len(hr_map[hr_dist_map[idx_i][d_list]])):
                    for pnt_m_m in range(0, len(hr_map[hr_dist_map[idx_i][d_list]])):
                        try:
                            hr_collected_map.index(hr_map[hr_dist_map[idx_i][d_list]][pnt_m_m])
                        except ValueError:
                            hr_collected_map.append(hr_map[hr_dist_map[idx_i][d_list]][pnt_m_m])
                            tmp_map.append(hr_map[hr_dist_map[idx_i][d_list]][pnt_m_m])
            hr_dist_map[idx_i + 1] = list(sorted(tmp_map))
        self.print_current_time("Completed Creating Map.")

        final_dist_arr = [-1] * n_ver
        for idx_i in range(1, n_ver):
            for idx_i_i in range(0, len(hr_dist_map[idx_i])):
                final_dist_arr[hr_dist_map[idx_i][idx_i_i]] = idx_i

        # for idx_i in range(1, n_ver):
        #     if final_dist_arr[idx_i] == -1 and idx_i != 1:
        #         idx_len = len(hr_map[idx_i])
        #         tmp_dist = n_ver
        #         if idx_len > 0:
        #             for pnt_m in range(0, idx_len):
        #                 if final_dist_arr[hr_map[idx_i][pnt_m]] != -1:
        #                     if tmp_dist > final_dist_arr[hr_map[idx_i][pnt_m]]:
        #                         tmp_dist = final_dist_arr[hr_map[idx_i][pnt_m]]
        #             final_dist_arr[idx_i] = tmp_dist + 1
        #
        #
        # for idx_i in range(1, n_ver):
        #     if final_dist_arr[idx_i] == -1:
        #         print(" %s | %s | " % (idx_i, hr_map[idx_i]))

        final_dist_arr[1] = 0
        result_str = " ".join(str(final_dist_arr[x]) for x in range(1, n_ver))
        self.write_content_to_file(file_name, str(result_str))
        print(result_str)

    def connected_components(self, file_name):
        self.print_current_time("Started Processing.")
        file_content = self.load_and_return_content(file_name)
        line_count = len(file_content)
        n_ver = int(file_content[0].split(" ")[0]) + 1
        m_edg_count = int(file_content[0].split(" ")[1])
        paird_node_list = [[int(x) for x in file_content[line_num].split(" ")] for line_num in range(1, line_count) if
                           len(file_content[line_num]) > 0]
        hr_map = [[] * n_ver for x in range(0, n_ver)]

        self.print_current_time("Started Processing Matrix.")
        for idx_i in range(0, m_edg_count):
            hr_map[paird_node_list[idx_i][0]].append(paird_node_list[idx_i][1])
            hr_map[paird_node_list[idx_i][1]].append(paird_node_list[idx_i][0])
        self.print_current_time("Completed Processing Matrix.")
        self.print_current_time("Started Creating Map.")
        fnl_cmp_lst = []
        exp_n_list = []
        c_idx = 0
        act_lst = n_ver - 1
        while len(exp_n_list) < act_lst:
            for idx_i in range(1, n_ver):
                try:
                    exp_n_list.index(idx_i)
                except ValueError:
                    c_idx = idx_i
                    exp_n_list.append(c_idx)
                    break
            hr_dist_map = [[] * n_ver for x in range(0, n_ver + 1)]
            hr_dist_map[1] = list(sorted(hr_map[c_idx]))
            hr_collected_map = [x for x in hr_dist_map[c_idx]]
            for idx_i in range(1, n_ver):
                tmp_map = []
                for d_list in range(0, len(hr_dist_map[idx_i])):
                    c_tmp_val = int(hr_dist_map[idx_i][d_list])
                    for pnt_m in range(0, len(hr_map[c_tmp_val])):
                        for pnt_m_m in range(0, len(hr_map[c_tmp_val])):
                            try:
                                hr_collected_map.index(hr_map[c_tmp_val][pnt_m_m])
                            except ValueError:
                                hr_collected_map.append(hr_map[c_tmp_val][pnt_m_m])
                                tmp_map.append(hr_map[c_tmp_val][pnt_m_m])
                hr_dist_map[idx_i + 1] = list(sorted(tmp_map))

            for idx_i in range(1, n_ver):
                for idx_i_i in range(0, len(hr_dist_map[idx_i])):
                    try:
                        exp_n_list.index(hr_dist_map[idx_i][idx_i_i])
                    except ValueError:
                        exp_n_list.append(hr_dist_map[idx_i][idx_i_i])
            try:
                exp_n_list.index(c_idx)
            except ValueError:
                exp_n_list.append(c_idx)
            exp_n_list = list(sorted(exp_n_list))
            try:
                hr_collected_map.index(c_idx)
            except ValueError:
                hr_collected_map.append(c_idx)
            fnl_cmp_lst.append(list(sorted(hr_collected_map)))
        self.print_current_time("Completed Creating Map.")
        result_str = str(len(fnl_cmp_lst))
        self.write_content_to_file(file_name, str(result_str))
        print(result_str)

        for x in range(0, len(fnl_cmp_lst)):
            print(fnl_cmp_lst[x])

    def max_heapify(self, elm_lst, i):
        l = 2 * i
        r = 2 * i + 1
        if l <= elm_lst[0] and elm_lst[l] >= elm_lst[i]:
            lrg = l
        else:
            lrg = i
        if r <= elm_lst[0] and elm_lst[r] > elm_lst[lrg]:
            lrg = r
        if lrg != i:
            elm_lst[i], elm_lst[lrg] = elm_lst[lrg], elm_lst[i]
            self.max_heapify(elm_lst, lrg)
        return elm_lst

    def building_a_heap(self, file_name):
        self.print_current_time("Started Processing.")
        file_content = self.load_and_return_content(file_name)
        elm_lst = [int(x) for x in file_content[1].split(" ")]
        elm_lst.insert(0, int(file_content[0]))
        hlf_n_elm = int(elm_lst[0] // 2)

        for node_x in range(0, hlf_n_elm):
            elm_lst = self.max_heapify(elm_lst, hlf_n_elm - node_x)

        good_call = 0
        bad_call = 0
        for node_x in range(0, hlf_n_elm):
            p_node = hlf_n_elm - node_x
            if 2 * p_node + 1 <= elm_lst[0]:
                if elm_lst[p_node] >= elm_lst[2 * p_node] and elm_lst[p_node] >= elm_lst[2 * p_node + 1]:
                    good_call += 1
                else:
                    bad_call += 1
            elif 2 * p_node <= elm_lst[0]:
                if elm_lst[p_node] >= elm_lst[2 * p_node]:
                    good_call += 1
                else:
                    bad_call += 1

        print(" Good Call -> " + str(good_call))
        print(" BAD Call -> " + str(bad_call))

        result_str = " ".join(str(elm_lst[x]) for x in range(1, elm_lst[0] + 1))
        self.write_content_to_file(file_name, str(result_str))

    def merge_sort_list(self, elm_lst):
        # print(elm_lst)
        if len(elm_lst) > 1:
            mid = len(elm_lst) // 2
            l_list = elm_lst[:mid]
            r_list = elm_lst[mid:]
            self.merge_sort_list(l_list)
            self.merge_sort_list(r_list)
            i = 0
            j = 0
            k = 0
            while i < len(l_list) and j < len(r_list):
                if l_list[i] < r_list[j]:
                    elm_lst[k] = l_list[i]
                    i += 1
                else:
                    elm_lst[k] = r_list[j]
                    j += 1
                # print("Loop# 1 %s " % (elm_lst))
                k += 1
            while i < len(l_list):
                elm_lst[k] = l_list[i]
                i += 1
                k += 1
                # print("Loop# 2 %s " % (elm_lst))
            while j < len(r_list):
                elm_lst[k] = r_list[j]
                j += 1
                k += 1
                # print("Loop# 3 %s " % (elm_lst))
        return elm_lst

    def merge_sort(self, file_name):
        self.print_current_time("Started Processing.")
        file_content = self.load_and_return_content(file_name)
        elm_lst = [int(x) for x in file_content[1].split(" ")]
        n_elm = int(file_content[0])
        elm_lst = self.merge_sort_list(elm_lst)
        result_str = " ".join(str(elm_lst[x]) for x in range(0, len(elm_lst)))
        self.write_content_to_file(file_name, str(result_str))
        print(result_str)

    def perfect_matchings_and_rna_secondary_structures(self, file_name):
        self.print_current_time("Started Processing.")
        result_arr = []
        content = self.load_and_return_content_delimted(file_name, ">")
        input_dict = {}
        for v_val in content:
            value_arr = v_val.split("\n")
            input_dict[value_arr[0]] = "".join([itm for itm in value_arr if len(itm) > 0 and itm != value_arr[0]])
        core_dict = {}
        input_keys = sorted(list(input_dict.keys()))
        opp_val_hash = {"A": "U", "U": "A", "G": "C", "C": "G"}
        nuc_key_list = list(sorted(list(opp_val_hash.keys())))
        matching_pair_one = ["A", "U"]

        for k_key in input_keys:
            core_dict[k_key] = {}
            core_dict[k_key]["seq"] = list(input_dict[k_key])
            cur_seq_rng = len(core_dict[k_key]["seq"])
            for nuc_idx in range(0, cur_seq_rng):
                key_val = None
                if core_dict[k_key]["seq"][nuc_idx] == "A":
                    key_val = "A"
                elif core_dict[k_key]["seq"][nuc_idx] == "G":
                    key_val = "G"
                elif core_dict[k_key]["seq"][nuc_idx] == "U":
                    key_val = "U"
                elif core_dict[k_key]["seq"][nuc_idx] == "C":
                    key_val = "C"
                try:
                    core_dict[k_key][key_val]
                except KeyError:
                    core_dict[k_key][key_val] = []
                core_dict[k_key][key_val].append(nuc_idx)

        pair_one_arr = []
        pair_two_arr = []
        for k_key in input_keys:
            core_dict[k_key]["possible_pair_two"] = len(core_dict[k_key]["G"])
            core_dict[k_key]["possible_pair_one"] = len(core_dict[k_key]["A"])
            for nuc_idx in range(0, len(nuc_key_list)):
                nuc_one_list = core_dict[k_key][nuc_key_list[nuc_idx]]
                nuc_two_list = core_dict[k_key][opp_val_hash[nuc_key_list[nuc_idx]]]
                c_list = [(nuc_x, nuc_y) for nuc_x in nuc_one_list for nuc_y in nuc_two_list]
                c_list_len = len(c_list)
                for tup_idx in range(0, c_list_len):
                    if c_list[tup_idx][1] < c_list[tup_idx][0]:
                        c_list[tup_idx] = (c_list[tup_idx][1], c_list[tup_idx][0])
                try:
                    matching_pair_one.index(nuc_key_list[nuc_idx])
                    for tup_idx in range(0, c_list_len):
                        try:
                            pair_one_arr.index(c_list[tup_idx])
                        except ValueError:
                            pair_one_arr.append(c_list[tup_idx])
                except ValueError:
                    for tup_idx in range(0, c_list_len):
                        try:
                            pair_two_arr.index(c_list[tup_idx])
                        except ValueError:
                            pair_two_arr.append(c_list[tup_idx])

        pair_one_limit = core_dict[k_key]["possible_pair_one"]
        pair_two_limit = core_dict[k_key]["possible_pair_two"]

        pair_one_result_arr = []
        for idx_i in range(0, len(pair_one_arr)):
            include_arr = []
            c_included = [pair_one_arr[idx_i]]
            for itm in pair_one_arr[idx_i]:
                include_arr.append(itm)
            for idx_i_i in range(0, len(pair_one_arr)):
                if idx_i != idx_i_i:
                    include = True
                    for itm in pair_one_arr[idx_i_i]:
                        try:
                            include_arr.index(itm)
                            include = False
                            break
                        except ValueError:
                            include = True
                    if include is True:
                        for itm in pair_one_arr[idx_i_i]:
                            include_arr.append(itm)
                        c_included.append(pair_one_arr[idx_i_i])
                if len(c_included) == pair_one_limit:
                    pair_one_result_arr.append(c_included)
                    include_arr = []
                    c_included = []
                    for itm in pair_one_arr[idx_i]:
                        include_arr.append(itm)
        pair_one_result_arr_dup = list(pair_one_result_arr)
        for idx_i in range(0, len(pair_one_result_arr_dup)):
            for idx_i_i in range(0, len(pair_one_result_arr_dup)):
                if idx_i != idx_i_i:
                    mat_count = 0
                    tup_rng = len(pair_one_result_arr_dup[idx_i])
                    for tup_x in range(0, tup_rng):
                        try:
                            pair_one_result_arr_dup[idx_i_i].index(pair_one_result_arr_dup[idx_i][tup_x])
                            mat_count += 1
                        except ValueError:
                            break
                    if mat_count == tup_rng:
                        try:
                            pair_one_result_arr.index(pair_one_result_arr_dup[idx_i])
                            try:
                                pair_one_result_arr.index(pair_one_result_arr_dup[idx_i_i])
                                pair_one_result_arr.remove(pair_one_result_arr_dup[idx_i_i])
                            except ValueError:
                                kk = 0
                        except ValueError:
                            kk = 0
                            # print("Found Match -> %s %s" % (pair_one_result_arr_dup[idx_i], pair_one_result_arr_dup[idx_i_i]))
        for idx_i in range(0, len(pair_one_result_arr)):
            print(pair_one_result_arr[idx_i])


            # print(pair_one_result_arr[idx_i])

            # for idx_i_i in range(0, len(pair_one_result_arr)):
            #     print(pair_one_result_arr)
            # for k_key in input_keys:
            #     p_val = core_dict[k_key]["possible_pair_one"] + 1
            #     temp_arr = pair_one_arr
            #     temp_val = "_int "
            #     init_str = ""
            #     end_str = ""
            #     if_statement = ""
            #     temp_arr_len = len(temp_arr)
            #     for i_val in range(1, p_val):
            #         init_str += " temp_arr[" + "i" * i_val + temp_val + "], "
            #         end_str += " for " + "i" * i_val + temp_val + " in range(0, temp_arr_len) "
            #         if_statement_temp = ""
            #         for x_val in range(1, p_val):
            #             if x_val != i_val:
            #                 if_statement_temp += " if " + "i" * i_val + temp_val + " != " + "i" * x_val + temp_val
            #         if_statement += if_statement_temp
            #     init_str = init_str[0:init_str.rindex(",")]
            #     init_str = "(" + init_str + ")"
            #     code_run = "out_val = [" + init_str + end_str + if_statement + "]"
            #     exec(code_run, locals())
            #     out_val_orig_one = list(locals()['out_val'])
            #     locals()['out_val'] = None
            #     out_val_orig_one_dup = list(out_val_orig_one)
            #     p_val = len(out_val_orig_one[0])
            #     temp_val = "_int "
            #     init_str = ""
            #     end_str = ""
            #     if_statement = ""
            #     for i_val in range(1, p_val + 1):
            #         init_str += " temp_arr[" + "i" * i_val + temp_val + "], "
            #         end_str += " for " + "i" * i_val + temp_val + " in range(0, p_val) "
            #         if_statement_temp = ""
            #         for x_val in range(1, p_val):
            #             if x_val != i_val:
            #                 if_statement_temp += " if " + "i" * i_val + temp_val + " != " + "i" * x_val + temp_val
            #         if_statement += if_statement_temp
            #     init_str = init_str[0:init_str.rindex(",")]
            #     init_str = "(" + init_str + ")"
            #     code_run = "out_val = [" + init_str + end_str + if_statement + "]"
            #     print(code_run)
            #     for tup_val in out_val_orig_one:
            #         temp_arr = tup_val
            #         exec(code_run, locals())
            #         temp_delete_arr = list(locals()['out_val'])
            #         locals()['out_val'] = None
            #         temp_delete_arr.remove(tup_val)
            #         print(len(out_val_orig_one_dup))
            #         for del_tuple in temp_delete_arr:
            #             out_val_orig_one_dup.remove(del_tuple)
            #         print(len(out_val_orig_one_dup))
            # print(len(out_val_orig))


            # print(pair_one_arr)
            # print(pair_two_arr)
            # print(core_dict)


            # for main_idx in range(0, input_range):
            #     cur_arr_len = len(core_dict[input_keys[main_idx]])
            #     for idx_i in range(0, cur_arr_len):
            #         c_list = [(idx_i, idx_i_i) for idx_i_i in range(0, cur_arr_len) if
            #                   core_dict[input_keys[main_idx]][idx_i_i] == opp_val_hash[
            #                       core_dict[input_keys[main_idx]][idx_i]]]
            #         c_list_len = len(c_list)
            #         for tup_idx in range(0, c_list_len):
            #             if c_list[tup_idx][1] < c_list[tup_idx][0]:
            #                 c_list[tup_idx] = (c_list[tup_idx][1], c_list[tup_idx][0])
            #         try:
            #             matching_pair_one.index(core_dict[input_keys[main_idx]][idx_i])
            #             for tup_idx in range(0, c_list_len):
            #                 try:
            #                     pair_one_arr.index(c_list[tup_idx])
            #                 except ValueError:
            #                     pair_one_arr.append(c_list[tup_idx])
            #         except ValueError:
            #             for tup_idx in range(0, c_list_len):
            #                 try:
            #                     pair_two_arr.index(c_list[tup_idx])
            #                 except ValueError:
            #                     pair_two_arr.append(c_list[tup_idx])
            #
            # print(pair_one_arr)
            # print(pair_two_arr)
            #    print(cur_arr)

    # ----------------------------------------  Incomplete
    #   ----------------------------------------  Incomplete
    #   ----------------------------------------  Incomplete

    def two_way_partition(self, file_name):
        self.print_current_time("Started Processing.")
        file_content = self.load_and_return_content(file_name)
        elm_count = int(file_content[0])
        elm_lst = [int(x) for x in file_content[1].split(" ")]
        one_elm = elm_lst[0]

        print(len(elm_lst))
        lrg_arr = []
        small_arr = []
        one_sam_lst = []
        for x in range(0, elm_count):
            if one_elm > elm_lst[x]:
                small_arr.append(elm_lst[x])
            elif one_elm < elm_lst[x]:
                lrg_arr.append(elm_lst[x])
            elif one_elm == elm_lst[x]:
                one_sam_lst.append(elm_lst[x])

        elm_lst = small_arr + one_sam_lst + lrg_arr

        # c_idx = 1
        # while elm_lst[c_idx] < one_elm:
        #     elm_lst[c_idx], elm_lst[elm_lst.index(one_elm)] = elm_lst[elm_lst.index(one_elm)], elm_lst[c_idx]
        #     c_idx += 1
        # self.print_current_time("Started 2nd Stage Sorting.")
        # c_idx = elm_lst.index(one_elm) + 1
        # while c_idx <= elm_count:
        #     if elm_lst[c_idx] < one_elm:
        #         l_element = elm_lst[c_idx]
        #         c_o_e_idx = elm_lst.index(one_elm)
        #         elm_lst.remove(l_element)
        #         elm_lst.insert(c_o_e_idx, l_element)
        #         c_idx = elm_lst.index(one_elm)
        #         # print("Found Small Value -> %s %s || %s || " % (l_element, one_elm, elm_lst[c_o_e_idx-5:c_o_e_idx+5]))
        #         # print("Found Small Value -> %s %s || %s || " % (l_element, one_elm, elm_lst[c_o_e_idx-5:c_o_e_idx+5]))
        #         # elm_lst.remove(l_element)
        #         # elm_lst.insert(c_o_e_idx-2, l_element)
        #         # c_idx = c_o_e_idx + 1
        #         # print("Found Small Value -> %s %s" % (c_o_e_idx, c_idx))
        #     else:
        #         c_idx += 1
        #     print(c_idx)
        # self.print_current_time("Completed 2nd Stage Sorting.")
        # # Confirmation

        c_idx = elm_lst.index(one_elm)
        for x in range(0, c_idx - 1):
            if one_elm <= elm_lst[x]:
                print(" DID NOT WORK. Wrong LOW value Location")
        for x in range(c_idx + 1, elm_count):
            if one_elm >= elm_lst[x]:
                print("DID NOT WORK. Higher Location Value -> %s %s " % (one_elm, elm_lst[x]))
        self.print_current_time("Confirmation Complete.")
        result_str = " ".join(str(elm_lst[x]) for x in range(0, len(elm_lst)))
        self.write_content_to_file(file_name, str(result_str))
        # print(result_str)
        # n_elm = int(file_content[0])

    def three_sum(self, file_name):
        self.print_current_time("Started Processing.")
        file_content = self.load_and_return_content(file_name)
        list_count = int(file_content[0].split(" ")[0])
        list_length = int(file_content[0].split(" ")[1]) - 1
        # large_number = 100001
        l_lists = [[int(x) for x in file_content[line_num].split(" ")] for line_num in range(1, list_count + 1) if
                   len(file_content[line_num]) > 0]
        result_arr = []
        for x in range(0, list_count):
            self.print_current_time("Started Processing.   " + str(x) + "   ")
            sorted_arr = sorted(l_lists[x])
            temp_arr = [-1, -1, -1]
            for i in range(0, list_length - 2):
                a = sorted_arr[i]
                start = i + 1
                end = list_length
                while start < end:
                    b = sorted_arr[start]
                    c = sorted_arr[end]
                    if a + b + c == 0:
                        temp_arr = [l_lists[x].index(a) + 1, l_lists[x].index(b) + 1, l_lists[x].index(c) + 1]
                        temp_arr = sorted(temp_arr)
                        break
                    elif a + b + c > 0:
                        end -= 1
                    else:
                        start += 1
                if temp_arr[2] != -1:
                    break

            result_arr.append(list(temp_arr))
        result_str = ""
        for x in range(0, len(result_arr)):
            if result_arr[x][2] == -1:
                tmp_str = str(result_arr[x][2])
            else:
                tmp_str = " ".join(str(k) for k in result_arr[x])
            result_str += "\n" + tmp_str
        self.write_content_to_file(file_name, str(result_str))
        print(result_str)



    def fallout_cheat(self, file_name):
        self.print_current_time("Started Processing.")
        file_content = self.load_and_return_raw_content(file_name)
        file_content = [c_val for c_val in file_content.split("\n") if len(c_val) > 0]
        c_arr = [list(c_val) for c_val in file_content]
        vowel_list = ["a", "e", "i", "o", "u"]
        count_arr = [[None for x in range(0, len(c_arr))] for y in range(0, len(c_arr))]
        lrg_match = 0
        for idx_i in range(0, len(c_arr)):
            idx_i_len = len(c_arr[idx_i])
            for idx_i_i in range(0, len(c_arr)):
                c_value = 0
                idx_i_i_len = len(c_arr[idx_i_i])
                for chr_i in range(0, idx_i_len):
                    if chr_i < idx_i_i_len:
                        if c_arr[idx_i][chr_i] == c_arr[idx_i_i][chr_i]:
                            c_value += 1
                count_arr[idx_i][idx_i_i] = str(c_value)
                if idx_i != idx_i_i and c_value > lrg_match:
                    lrg_match = c_value
        result_arr = [None for x in range(0, len(c_arr)+2)]
        result_arr[0] = "|" + "|".join(file_content)
        for x_val in range(0, len(count_arr)):
            result_arr[x_val+1] = "%s|%s" % (file_content[x_val], "|".join(count_arr[x_val]))
        result_arr[len(count_arr)+1] = str(lrg_match)
        result_str = "\n".join(result_arr)
        print(result_str)



        # print(c_arr)


    def parse_sample_file(self):
        input_file_address = "Removed"
        content = self.filefoldermanager.read_file_to_memory(input_file_address)
        lines = content.split("\n")
        reg_x = re.compile('(.*)([ ]{1})([\w]{3})([ ]{1})([ ]{1}\d{1}|\d{2})([ ]{1})(.*)')
        reg_x_two = re.compile('(\d{2}[:]{1}\d{2}[ ]{1})(.*)')

        fld_address = "Removed"
        output_file = "Removed"

        for line in lines:
            rst = reg_x.search(line)
            if rst and str(rst.group(3)) == "Nov" and str(rst.group(5)) == "14":
                    lft_most = str(rst.group(7))
                    rst_two = reg_x_two.search(lft_most)
                    if rst_two:
                        file_name = fld_address + str(rst_two.group(2))
                        prt_str = "cat %s >> %s " % (file_name, output_file)
                        print("echo \"\n\n %s \" >> %s " % (str(rst_two.group(2)), output_file))
                        print(prt_str)




    def parse_upload_folder_list(self):
        input_file_address = "Removed"
        content = self.filefoldermanager.read_file_to_memory(input_file_address)
        lines = content.split("\n")
        reg_x = re.compile('(.*)([ ]{1})([\w]{3})([ ]{1})([ ]{1}\d{1}|\d{2})([ ]{1})(.*)')

        for line in lines:
            rst = reg_x.search(line)
            if rst and str(rst.group(3)) == "Nov" and str(rst.group(5)) == "14":
                print(line)

    def image_proc(self):

        one_file = "Removed"
        one_image = Image.open(one_file)
        one_image.load()
        one_image_prop = list(one_image.getdata())
        new_file = Image.new("RGB", one_image.size)
        new_file.putdata(one_image_prop)
        new_file.show()


        # im = Image.new("RGB", (20, 20), "blue")
        # im.show()
        #
        # im_data = list(im.getdata())
        #
        # for idx_i in range(0, len(im_data)):
        #         print("%s | %s" % (idx_i, im_data[idx_i]))


        # zero_file = "C:\\Users\\mhaque8672\\Desktop\\images\\Zero.jpg"
        # one_file = "C:\\Users\\mhaque8672\\Desktop\\images\\One.jpg"
        #
        # zero_image = Image.open(zero_file)
        # zero_image.load()
        # zero_image_prop = list(zero_image.getdata())
        #
        # one_image = Image.open(one_file)
        # one_image.load()
        # one_image_prop = list(one_image.getdata())
        #
        #
        #
        # for idx_i in range(0, len(zero_image_prop)):
        #     if zero_image_prop[idx_i] != one_image_prop[idx_i]:
        #         print("%s | %s" % (zero_image_prop[idx_i], one_image_prop[idx_i]))


        # print(zero_image.getextrema())


        # a = Image.new("RGB", (512, 512), "red")
        # a.show()


    def testing_bipartiteness(self, file_name):
        self.print_current_time("Started Processing.")
        file_content = self.load_and_return_raw_content(file_name)
        file_content = file_content.split("\n\n")
        grp_count = int(file_content[0])


        grp_list = [[[int(xk_val) for xk_val in k_val.split(" ")] for k_val in file_content[v_val + 1].split("\n")] for
                    v_val in range(0, grp_count)]
        for x_val in range(0, grp_count):
            # Collect all the data_points
            dp_count = grp_list[x_val][0][0]
            edg_count = grp_list[x_val][0][1]
            edg_arr = [[] for x in range(0, dp_count+1)]
            # Map all the items to their fields.
            for t_tup in range(1, edg_count + 1):
                edg_arr[grp_list[x_val][t_tup][0]].append(grp_list[x_val][t_tup][1])
                edg_arr[grp_list[x_val][t_tup][1]].append(grp_list[x_val][t_tup][0])


            # Count and check if they share vertices.
            # Do this for every point in that list.
                # Step-1 : Pick one point.
                # Step-2 : load that points connections.
                # Step-3 : Compare the connection of picked with that of the current points.

            for edg in range(1, dp_count+1):
                print(edg_arr[edg])
            print(edg_arr)
            #
            #
            #
            # print(edg_arr)

        # a = numpy.array([1, 2, 3])
        # print(type(a))
               # print(grp_list[x_val][t_tup])
            # print(edg_arr)

            # print(grp_list[x_val])


        # print(grp_list)


if __name__ == "__main__":
    main_class = SolveRoselindProblems()
    # main_class.create_class_name_items("Testing Bipartiteness", "bip")


    main_class.image_proc()

    # main_class.fallout_cheat("fallout_pass.txt")
    # main_class.testing_bipartiteness("rosalind_bip.txt")
    # main_class.three_sum("rosalind_3sum.txt")
    # main_class.two_way_partition("rosalind_par.txt")
    # INCOMPLETE main_class.perfect_matchings_and_rna_secondary_structures("rosalind_pmch.txt")
    # main_class.merge_sort("rosalind_ms.txt")
    # main_class.building_a_heap("rosalind_hea.txt")
    # main_class.connected_components("rosalind_cc.txt")
    # main_class.breadth_first_search("rosalind_bfs.txt")
    # main_class.algorithm_2sum("rosalind_2sum.txt")
    # main_class.test_sorting("sample_sort.txt")
    # main_class.merge_two_sorted_arrays("rosalind_mer.txt")
    # main_class.test_recurse(1)
    # main_class.majority_element("rosalind_maj.txt")
    # main_class.doubledegree_array("rosalind_ddeg.txt")
    # main_class.insertion_sort("rosalind_ins.txt")
    # main_class.degree_array("rosalind_deg.txt")
    # main_class.binary_search("rosalind_bins.txt")
    # main_class.fibonacci_numbers("rosalind_fibo.txt")
    # main_class.count_nucleotide_from_file("rosalind_dna.txt")
    # main_class.convert_dna_to_rna("rosalind_rna.txt")
    # main_class.generate_complementing_strand_of_dna("rosalind_revc.txt")
    # main_class.generate_rabbits_and_recurrence_relations("rosalind_fib.txt")
    # main_class.generate_mortal_fibonacci_rabbits("rosalind_fibd.txt")
    # main_class.count_g_c_content("rosalind_gc.txt")
    # main_class.find_counting_point_mutations("rosalind_hamm.txt")
    # main_class.find_mandels_law("rosalind_iprb.txt")
    # main_class.generate_rna_to_protein("rosalind_prot.txt")
    # main_class.find_dna_motif("rosalind_subs.txt")
    # main_class.create_dna_profile("rosalind_cons.txt")
    # main_class.loop_test()
    # main_class.create_overlap_graphs("rosalind_grph.txt")
    # main_class.calculating_expected_offspring("rosalind_iev.txt")
    # main_class.finding_a_shared_motif("rosalind_lcsm.txt")
    # main_class.independent_alleles("rosalind_lia.txt")
    # main_class.finding_a_protein_motif("rosalind_mprt.txt")
    # main_class.inferring_mrna_from_protein("rosalind_mrna.txt")
    # main_class.open_reading_frames("rosalind_orf.txt")
    # main_class.enumerating_gene_orders("rosalind_perm.txt")
    # main_class.calculating_protein_mass("rosalind_prtm.txt")
    # main_class.locating_restriction_sites("rosalind_revp.txt")
    # main_class.rna_splicing("rosalind_splc.txt")
    # main_class.enumerating_k_mers_lexicographically("rosalind_lexf.txt")
    # main_class.longest_increasing_subsequence("rosalind_lgis.txt")
    # main_class.genome_assembly_as_shortest_superstring("rosalind_long.txt")
