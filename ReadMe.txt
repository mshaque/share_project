solve_rosalind_problems.py -> Contains all the solving methods. 
helper_scripts -> Contains scripts that help loading file or connect to database. Generic useful functions.
roselind_problems -> Contains the problems downloaded from rosalind.
roselind_results -> Contains the solutions that were uploaded. 

My user page at rosalind ( may help relate the methods to the problems they are solving.)

http://rosalind.info/users/Mhaque/
