class FormatValue:

    def list_to_string_delim_linebreak(self, content):
        print_str = ""
        line_count = len(content)
        for index_i in range(line_count):
            if index_i != line_count:
                print_str += str(content[index_i]) + "\n"
            else:
                print_str += str(content[index_i])
        return print_str
