class PrintToConsole:

    @staticmethod
    def print_table(value_list, order_list=[]):
        final_column_name = []
        if len(order_list) == 0:
            for itm in value_list:
                filtered_list = [col_name for col_name in list(itm) if col_name not in final_column_name]
                if len(filtered_list) > 0:
                    final_column_name += filtered_list
            final_column_name.sort()
        else:
            final_column_name = order_list
        longest_column_length = 0
        for itm in final_column_name:
                if len(itm) > longest_column_length:
                    longest_column_length = len(itm)
        for val_dict in value_list:
            for k_key in final_column_name:
                itm_len = len(str(val_dict.get(k_key, 0)))
                if itm_len > longest_column_length:
                    longest_column_length = itm_len
        final_out_arr = []
        temp_fld_str = ""
        longest_column_length += 2
        for itm in final_column_name:
            itm_len = len(itm)
            if itm_len <= longest_column_length:
                temp_fld_str += "| " + itm + " " * (longest_column_length - itm_len)
        final_out_arr.append(temp_fld_str)
        for val_dict in value_list:
            temp_fld_str = ""
            for k_key in final_column_name:
                itm_len = len(str(val_dict.get(k_key, 0)))
                if itm_len <= longest_column_length:
                    temp_fld_str += "| " + str(val_dict.get(k_key, 0)) + " " * (longest_column_length - itm_len)
            final_out_arr.append(temp_fld_str)
        result_table = "\n".join(final_out_arr)
        return result_table
