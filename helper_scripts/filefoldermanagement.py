class FileFolderManagement:

    @staticmethod
    def read_file_to_memory(file_address):
        f = open(file_address, 'r+')
        contents = f.read()
        return contents

    @staticmethod
    def read_file_into_array_by_line(file_address):
        contents = FileFolderManagement.read_file_to_memory(file_address)
        file_arr = contents.split("\n")
        file_arr_copy = list(file_arr)
        line_count = len(file_arr_copy)
        for index_i in range(line_count):
            if len(file_arr_copy[index_i]) == 0:
                    if file_arr_copy[index_i] in file_arr:
                        file_arr.remove(file_arr_copy[index_i])
        return file_arr

    @staticmethod
    def read_file_into_array_by_delim(file_address, delim):
        contents = FileFolderManagement.read_file_to_memory(file_address)
        file_arr = contents.split(delim)
        file_arr_copy = list(file_arr)
        line_count = len(file_arr_copy)
        for index_i in range(line_count):
            if len(file_arr_copy[index_i]) == 0:
                    if file_arr_copy[index_i] in file_arr:
                        file_arr.remove(file_arr_copy[index_i])
        return file_arr

    @staticmethod
    def create_folder_structure(folder_address):
        os.makedirs(folder_address, exist_ok=True)

    @staticmethod
    def generate_file(file_address, over_write):
        if script_properties['platform'] == "Windows":
            file_address = file_address.replace("/", "\\")
            folder_address = file_address[:file_address.rindex("\\")]
        else:
            folder_address = file_address[:file_address.rindex("\\")]
        FileFolderManagement.create_folder_structure(folder_address)
        if os.path.exists(file_address):
            if over_write:
                os.remove(file_address)


    @staticmethod
    def write_content_to_file(content, file_address, over_write=False):
        FileFolderManagement.generate_file(file_address, over_write)
        if over_write:
            target = open(file_address, 'w+')
        else:
            target = open(file_address, 'a')
        target.write(content)
        target.close()
