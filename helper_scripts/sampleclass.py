

class SampleClass:
    def __init__(self):
        print("Sample Class is  Loading......")
 
    @staticmethod
    def sample_method_1():
        print("This is sample Method 1")
 
    @staticmethod
    def sample_method_2():
        print("This is Sample Method 2.")
 
 
    @staticmethod
    def sample_method_3():
        print("This is Sample Method 3")
 
    @staticmethod
    def sample_method_4():
        print("This is Sample Method 4")
 
    @staticmethod
    def sample_method_5():
        print("This is Sample Method 5")

    @staticmethod
    def inspect_main_module():

        pval = sys.modules['__main__'].__dict__['array']#.__dict__['parse']
        for itm in pval.__dict__.keys():
            print(itm)