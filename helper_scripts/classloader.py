import sys
import os
import re
import importlib
import platform

script_properties = {}


class ClassLoader:
    req_dict = {}

    def __init__(self):
        self.reconfigure_path()
        script_properties["python_location"] = sys.path
        script_properties['scr_loc'] = os.path.abspath(__file__)
        script_properties['scr_loc'] = script_properties['scr_loc'][:script_properties['scr_loc'].rfind("\\")]
        sys.path.append(script_properties['scr_loc'])
        script_properties['resource_location'] = script_properties['scr_loc'][:script_properties['scr_loc'].rfind(
            "\\helper_scripts")] + "\\resources"
        script_properties['load_sys_library'] = ["io", "json", "math", "random", "datetime", "time", "requests", "numbers"]
        script_properties['load_Individual_sys_packages'] = [["PIL.Image", "Image"],
                                                             ["PIL.ImageFilter", "ImageFilter"]]

        # "sympy", "numpy"]  # "http", "urllib"]  # "mysql", "json", "http", "urllib", "array" , "hardware"
        script_properties['load_local_library'] = []
        script_properties['resources'] = {}
        script_properties['platform'] = platform.system()

    @staticmethod
    def reconfigure_path():
        path_arr = sys.path
        # for itm in path_arr:
        #     if itm.rfind("\\lib") >= 1:
        #         sys.path.remove(itm)
        #     if itm.rfind("/lib") >= 1:
        #         sys.path.remove(itm)
        sys.path.append(__file__[:__file__.rindex("helper_scripts")] + "\Lib")
        # sys.path.append(__file__[:__file__.rindex("helper_scripts")] + "\Lib")
        # os.path.append(__file__)
        # for num in range(len(sys.path)):
        #     print(str(num) + " -- > " + sys.path[num])

    def pre_run_script_prep(self):
        self.create_local_library_list()
        self.load_current_script_libraries()
        self.load_py_libraries()
        self.load_py_individual_from_packages()
        self.load_local_libraries()
        self.load_modules_to_loader()
        self.load_json_resource()
        self.load_main_module()
        self.load_helper_modules()
        self.fix_file_addresses()
        print("loaded pre run script.")

    @staticmethod
    def create_local_library_list():
        only_files = [f for f in os.listdir(script_properties['scr_loc']) if
                      os.path.isfile(os.path.join(script_properties['scr_loc'], f))]
        for k_key in only_files:
            if k_key.find(".py") >= 0 and 'helper_scripts.' + k_key != __name__ + ".py":
                script_properties['load_local_library'].append(k_key[:k_key.rindex(".py")])

    @staticmethod
    def load_current_script_libraries():
        for k_key in sys.modules[__name__].__dict__.keys():
            if k_key.find("__") != 0:
                ClassLoader.req_dict[k_key] = sys.modules[__name__].__dict__[k_key]

    def load_py_individual_from_packages(self):
        for mod2load in script_properties['load_Individual_sys_packages']:
            ClassLoader.req_dict[mod2load[1]] = self.return_loaded_module(mod2load[0])

    def load_py_libraries(self):
        for mod2load in script_properties['load_sys_library']:
            ClassLoader.req_dict[mod2load] = self.return_loaded_module(mod2load)
            key_list = ClassLoader.req_dict[mod2load].__dict__.keys()
            if '__path__' in key_list:
                mod_address = ClassLoader.req_dict[mod2load].__dict__['__path__'][0]
                folder_list = self.return_folder_list(mod_address)
                for fld_name in folder_list:
                    temp_mod_name = mod2load + "." + fld_name
                    ClassLoader.req_dict[mod2load].__dict__[fld_name] = self.return_loaded_module(temp_mod_name)
                file_list = self.return_file_list(mod_address)
                for fld_name in file_list:
                    if fld_name != '__init__':
                        temp_mod_name = mod2load + "." + fld_name
                        ClassLoader.req_dict[mod2load].__dict__[fld_name] = self.return_loaded_module(temp_mod_name)

    @staticmethod
    def return_loaded_module(mod2load):
        temp_module = importlib.import_module(mod2load, None)
        return temp_module

    @staticmethod
    def return_file_list(dir_address):
        only_files = [f for f in os.listdir(dir_address) if os.path.isfile(os.path.join(dir_address, f))]
        output_module_name = []
        py_finder = re.compile(r'(.*)(.py)')
        for k_key in only_files:
            matcher = py_finder.match(k_key)
            if matcher and 'helper_scripts.' + matcher.group(1) != __name__:
                output_module_name.append(matcher.group(1))

        return output_module_name

    @staticmethod
    def return_folder_list(dir_address):
        ignore_fld_list = ['__pycache__', 'test']
        only_folders = [f for f in os.listdir(dir_address) if not os.path.isfile(os.path.join(dir_address, f))]
        for fld_name in only_folders:
            if fld_name in ignore_fld_list:
                only_folders.remove(fld_name)
        return only_folders

    @staticmethod
    def load_local_libraries():
        for mod2load in script_properties['load_local_library']:
            ClassLoader.req_dict[mod2load] = importlib.import_module(mod2load, None)

    @staticmethod
    def print_module_content(cur_module):
        for itm in cur_module.__dict__.keys():
            # pVal   = cur_module[itm]
            print(cur_module.__name__ + " --> " + itm)  # + " --> " +  cur_module[itm] )

    @staticmethod
    def load_modules_to_loader():
        for mod2load in ClassLoader.req_dict:
            sys.modules[__name__].__dict__[mod2load] = ClassLoader.req_dict[mod2load]

    @staticmethod
    def load_main_module():
        for mod2load in ClassLoader.req_dict:
            sys.modules['__main__'].__dict__[mod2load] = ClassLoader.req_dict[mod2load]

    @staticmethod
    def load_helper_modules():
        for curModule in script_properties['load_local_library']:
            for mod2load in ClassLoader.req_dict:
                sys.modules[curModule].__dict__[mod2load] = ClassLoader.req_dict[mod2load]

    @staticmethod
    def load_json_resource():
        f = filefoldermanagement.FileFolderManagement()
        only_files = [f for f in os.listdir(script_properties['resource_location']) if
                      os.path.isfile(os.path.join(script_properties['resource_location'], f))]
        resource_finder = re.compile(r'(.*)(.json)')
        for k_key in only_files:
            matcher = resource_finder.match(k_key)
            if matcher:
                file_address = script_properties['resource_location'] + "/" + k_key
                content = f.read_file_to_memory(file_address)
                json_data = json.loads(content)
                script_properties['resources'][matcher.group(1)] = json_data
                # print(json_data)
                # script_properties['resources'].append(matcher.group(1))

    @staticmethod
    def fix_file_addresses():
        key_list = script_properties["resources"]["file_folder_address"].keys()
        for itm in key_list:
            file_address = __file__[:__file__.rindex("helper_scripts")] + \
                           script_properties["resources"]["file_folder_address"][itm]
            file_address = ClassLoader.sanitize_file_address(file_address)
            script_properties["resources"]["file_folder_address"][itm] = file_address

    @staticmethod
    def sanitize_file_address(file_address):
        if script_properties['platform'] == "Windows":
            file_address = file_address.replace("/", "\\")
            return file_address


ClassLoader().pre_run_script_prep()



# print(k_key + " --> " + str(k_key.find("__")))
# if not k_key.index("__") != 0:
#     ClassLoader.req_dict[k_key] = sys.modules[__name__].__dict__[k_key]


# src_path = __file__
# src_path = src_path.replace("helper_scripts\\" + __name__ + ".py", "")
# src_path = src_path.replace("helper_scripts/" + __name__ + ".py", "")

# if src_path.rindex("//helper_scripts") != -1:
#     print(src_path)

# print(src_path)

# if src_path.rindex("\\helper_scripts\\") >= 0:
#     print(src_path)

# print(self.__dict__['sys']['path'])

# scr_path = sys.modules['__main__'].__file__
# scr_path = scr_path.replace("/", "||")

# scr_path = os.path.dirname(scr_path) + "/" + os.path.basename(scr_path)

# print(scr_path)
# sys.path.append(scr_path)
# path_arr = sys.path
# for itm in path_arr:
#         print(itm + " -- " + str(len(itm)))

# print(scr_path)

# path_arr = sys.path
# mod_regex = re.compile(r'(.*)(\\lib)(.*)')
# for v_val in path_arr:
#     matcher = mod_regex.match(v_val)
#     if matcher:
#         sys.path.remove(v_val)
# mod_regex = re.compile(r'(.*)(/lib)(.*)')
# for v_val in path_arr:
#     matcher = mod_regex.match(v_val)
#     if matcher:
#         sys.path.remove(v_val)
# scr_path = sys.modules['__main__'].__file__
# lib_path = ''
# if scr_path[:scr_path.rfind("\\")] != -1:
#     lib_path = scr_path[:scr_path.rfind("\\")] + "\\Lib"
# if scr_path[:scr_path.rfind("/")] != -1:
#     lib_path = scr_path[:scr_path.rfind("/")] + "/Lib"
# lib_path = lib_path.replace("/", "\\")
#
#
# print(lib_path)
# print()

# sys.path.append("C:\\Users\\mhaque8672\\PycharmProjects\\py_project_one\\Lib")

# for num in range(len(sys.path)):
#     print(str(num) + " -- > " + sys.path[num])
