

class DBConn:


    @staticmethod
    def return_connection(self,config):
            try:
                cnx = sys.modules['mysql.connector'].connect(**config)
                return cnx
            except:
                print("connection error")
                exit(1)


    def return_cursor(self):
        cnx = self.return_connection()
        return cnx.cursor()


    @staticmethod
    def create_database(self,cnx , db_name ):
        #cnx = self.return_connection()
        cursor = cnx.cursor()

        try:
           cursor.execute("CREATE DATABASE {} DEFAULT CHARACTER SET 'utf8'".format(db_name))
           # cursor.execute(' DROP DATABASE ' + db_name )
        except mysql.connector.Error as err:
            print("Failed creating database: {}".format(err))
            exit(1)

    @staticmethod
    def drop_database(self,cnx , db_name ):
        #cnx = self.return_connection()
        cursor = cnx.cursor()

        try:
           cursor.execute(' DROP DATABASE ' + db_name )
        except mysql.connector.Error as err:
            print("Failed creating database: {}".format(err))
            exit(1)













