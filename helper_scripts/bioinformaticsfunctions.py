class BioinformaticsFunctions:
    @staticmethod
    def count_nucleotide(sequence):
        count_hash = {"A": 0, "C": 0, "G": 0, "T": 0}
        char_arr = list(sequence)
        for char in char_arr:
            count_hash[char] += 1
        return count_hash

    @staticmethod
    def convert_to_rna(sequence):
        sequence = sequence.replace("T", "U")
        return sequence

    @staticmethod
    def create_complementing_strand_of_dna(sequence):
        comp_list = {"A": "T", "C": "G", "G": "C", "T": "A"}
        nuc_list = list(sequence)
        result_str = ""
        total_nuc = len(nuc_list)
        for nuc in range(0, total_nuc):
            result_str += comp_list[nuc_list[(total_nuc - nuc) - 1]]
        return result_str

    def create_reverse_complementing_strand_of_dna(self, sequence):
        sequence = "".join(reversed(list(sequence)))
        result_str = self.create_complementing_strand_of_dna(sequence)
        result_str = "".join(reversed(list(result_str)))
        return result_str

    @staticmethod
    def return_rabbits_and_recurrence_relations(months, litter):
        record_hash = []
        row_count = {"month": 1, "old_rabbit": 0, "new_rabbit": 1, "total_rabbit": 1}
        record_hash.append(dict(row_count))
        while row_count["month"] < months:
            row_count["month"] += + 1
            row_count["new_rabbit"] = row_count["old_rabbit"] * litter
            row_count["old_rabbit"] = row_count["total_rabbit"]
            row_count["total_rabbit"] = row_count["old_rabbit"] + row_count["new_rabbit"]
            record_hash.append(dict(row_count))
        return printtoconsole.PrintToConsole.print_table(record_hash)

    @staticmethod
    def return_mortal_fibonacci_rabbits(months, longevity):
        litter = 1
        record_hash = []
        new_rabbit_count = {}
        row_count = {"month": 1,
                     "old_rabbit": 0,
                     "new_rabbit": 1,
                     "dead_rabbits": 0,
                     "total_rabbit": 1}
        order_list = ['month', 'old_rabbit', 'new_rabbit', 'dead_rabbits', 'total_rabbit']
        record_hash.append(dict(row_count))
        while row_count["month"] < months:
            new_rabbit_count[row_count["month"]] = row_count["new_rabbit"]
            row_count["month"] += + 1
            row_count["new_rabbit"] = row_count["old_rabbit"] * litter
            row_count["old_rabbit"] = row_count["total_rabbit"] - new_rabbit_count.get(row_count["month"] - longevity, 0)
            row_count["dead_rabbits"] = new_rabbit_count.get(row_count["month"] - longevity, 0)
            row_count["total_rabbit"] = row_count["old_rabbit"] + row_count["new_rabbit"]
            record_hash.append(dict(row_count))
        return printtoconsole.PrintToConsole.print_table(record_hash, order_list)


    @staticmethod
    def rna_to_protein(sequence):
        char_count = 3
        codon_list = [sequence[i:i+char_count] for i in range(0, len(sequence), char_count)]
        protein_str = ""
        for codon in codon_list:
            if len(codon) == 3:
                protein_str += script_properties["resources"]["protein_mapped"][codon]["one_letter_name"]
        return protein_str

    @staticmethod
    def create_offspring_array(parent_comb):
        off_spring_arr = []
        parent_arr = []
        for parent in parent_comb:
            parent_arr.append(list(parent))
        parent_one = parent_arr[0]
        parent_two = parent_arr[1]
        for p_one_type in parent_one:
            for p_two_type in parent_two:
                off_spring_arr.append(p_one_type + p_two_type)
        return off_spring_arr

    @staticmethod
    def create_independent_allele_offspring_array(parent_comb):
        off_spring_arr = []
        parent_arr = []
        for parent in parent_comb:
            parent_arr.append(list(parent))
        parent_one = parent_arr[0]
        parent_two = parent_arr[1]
        for p_one_type in parent_one:
            for p_two_type in parent_two:
                    off_spring_arr.append(p_one_type + p_two_type)
        off_spring_unique_arr = []
        for idx_i in range(0, len(off_spring_arr)):
            try:
                off_spring_unique_arr.index(off_spring_arr[idx_i])
            except ValueError:
                off_spring_unique_arr.append(off_spring_arr[idx_i])
        return off_spring_unique_arr

    # @staticmethod
    # def return_factorial(provided_value):
    #     removed_value = provided_value - 1
    #     calculated_val = provided_value
    #     while removed_value >= 1:
    #         calculated_val *= removed_value
    #         removed_value -= 1
    #     return calculated_val



